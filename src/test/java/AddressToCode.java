
import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.dwarf.DwarfLine;
import java.io.File;
import java.util.ArrayList;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class AddressToCode {

	@Test
	public void test() {
//		final ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(new File(ParseRiscVElf.class.getResource("/riscv/xv6/kernel").getPath()), 0, false);
		final ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(new File("/opt/riscv-tests/share/riscv-tests/isa/rv64ui-v-addi"), 0, true);
		for (Dwarf dwarf : dwarfArrayList) {
			for (CompileUnit cu : dwarf.compileUnits) {
				System.out.println(cu);
				System.out.println(Long.toHexString(cu.DW_AT_low_pc) + " < " + Long.toHexString(cu.DW_AT_low_pc + cu.DW_AT_high_pc));
				if (cu.DW_AT_low_pc <= 0x80000020l && 0x80000020l <= (cu.DW_AT_low_pc + cu.DW_AT_high_pc)) {
					for (DwarfLine line : cu.dwarfDebugLineHeader.lines) {
//						System.out.println(cu.dwarfDebugLineHeader.filenames.get((int) line.file_num).file.getAbsolutePath() + "\t" + line);
						System.out.println(line.address.toString(16));
					}
				}
			}
		}
	}
}

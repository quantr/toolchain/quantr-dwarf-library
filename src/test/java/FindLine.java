
import hk.quantr.dwarf.DwarfGlobal;
import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.DebugInfoEntry;
import hk.quantr.dwarf.dwarf.Definition;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.dwarf.DwarfLine;
import hk.quantr.dwarf.helper.VariableWithAddress;
import java.io.File;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;
import org.junit.Test;

public class FindLine {

	@Test
	public void test1() {
		DwarfGlobal.debug = true;
		File file = new File(FindLine.class.getResource("/riscv/xv6/kernel").getPath());
		final ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(file, 0, true);
//		for (Dwarf dwarf : dwarfArrayList) {
//			for (CompileUnit cu : dwarf.compileUnits) {
//				System.out.println(cu);
//				for (DwarfLine line : cu.dwarfDebugLineHeader.lines) {
//					System.out.println(cu.dwarfDebugLineHeader.filenames.get((int) line.file_num).file.getAbsolutePath() + "\t" + line);
//				}
//			}
//		}

		long address = 0x10058;
		List<VariableWithAddress> variableWithAddresses = new ArrayList<VariableWithAddress>();

		CompileUnit cu = DwarfLib.getCompileUnit(dwarfArrayList, address);
		for (DwarfLine line : cu.dwarfDebugLineHeader.lines) {
			if (line.address.compareTo(BigInteger.valueOf(address)) < 0) {
				continue;
			}
			System.out.println(line.file_num + " = " + cu.dwarfDebugLineHeader.filenames.get((int) line.file_num).file.getAbsolutePath() + "\t" + line);
			VariableWithAddress variableWithAddress = new VariableWithAddress();
			variableWithAddress.file_num = line.file_num;
			variableWithAddress.line_num = line.line_num;
			variableWithAddress.address = line.address;
			variableWithAddresses.add(variableWithAddress);
		}

		for (DebugInfoEntry debugInfoEntry : cu.debugInfoEntries) {
			for (DebugInfoEntry debugInfoEntry2 : debugInfoEntry.debugInfoEntries) {
				if (debugInfoEntry2.name.equals("DW_TAG_variable")) {
				} else if (debugInfoEntry2.name.equals("DW_TAG_subprogram")) {
					for (DebugInfoEntry debugInfoEntry3 : debugInfoEntry2.debugInfoEntries) {
						if (debugInfoEntry3.name.equals("DW_TAG_variable")) {
//								System.out.println("\t\t\t\t" + debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_name"));
//								System.out.println("\t\t\t\t" + debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_decl_file"));
//								System.out.println("\t\t\t\tclass=" + debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_decl_line"));
//								System.out.println("\t\t\t\tclass=" + debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_decl_line").value.getClass());

							int lineNo;
							if (debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_decl_line").form == Definition.DW_FORM_data1) {
								lineNo = Integer.parseInt(debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_decl_line").value.toString());
							} else {
								lineNo = Integer.parseInt(debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_decl_line").value.toString(), 16);
							}
							int fileNo = (int) debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_decl_file").value;
//								BigInteger tempAddress = null;

							List<VariableWithAddress> result = variableWithAddresses.stream().filter(a -> a.file_num == fileNo).collect(Collectors.toList());

							boolean check = false;
							if (result.get(0).line_num <= lineNo && lineNo <= result.get(result.size() - 1).line_num) {
								check = true;
							}
							if (!check) {
								return;
							}

							System.out.println("\t\t\t\t" + fileNo + " , " + lineNo + " = " + debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_name").value);
//System.out.print("\t\t\t\t" + debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_decl_file").value);

//								for (VariableWithAddress v : result) {
//									if (v.address.compareTo(BigInteger.valueOf(address)) >= 0) {
//										System.out.print("\t\t\t\t" + fileNo + " , " + lineNo + " = " + debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_name").value);
//										//System.out.print("\t\t\t\t" + debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_decl_file").value);
//										System.out.println(" : " + debugInfoEntry3.debugInfoAbbrevEntries.get("DW_AT_decl_line").value);
//									}
//								}
							//System.out.println("--------------------------------------------");
						}
					}
				}
			}
		}
	}
}

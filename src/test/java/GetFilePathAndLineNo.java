
import hk.quantr.dwarf.QuantrDwarf;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.helper.VariableWithAddress;
import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class GetFilePathAndLineNo {

	@Test
	public void test() {
		ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(new File("../../2TB/workspace/xv6-riscv/kernel/kernel"), 0, false);
		VariableWithAddress v = QuantrDwarf.getFilePathAndLineNo(dwarfArrayList, BigInteger.valueOf(0x80005d9el));
		System.out.println(v.file.getName() + ": " + v.line_num);
	}
}

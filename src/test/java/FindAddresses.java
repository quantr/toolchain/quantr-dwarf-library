
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

public class FindAddresses {

	public static void main(String args[]) {
		final ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(new File("../PeterI/kernel/kernel"), 0, false);
		System.out.println(dwarfArrayList.size());
		HashMap<Integer, BigInteger> ht = DwarfLib.getAddresses(dwarfArrayList, "kernel.cpp");
		for (Integer key : ht.keySet()) {
			System.out.println("Value of " + key + " is: " + ht.get(key));
		}
	}
}

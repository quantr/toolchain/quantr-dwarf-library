
import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.dwarf.DwarfLine;
import hk.quantr.dwarf.helper.VariableWithAddress;
import java.io.File;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestXVKernel {

	@Test
	public void test1() {
//		DwarfGlobal.debug = true;
//		File file = new File(System.getProperty("user.home") + "/workspace/xv6-riscv/kernel/kernel");
		File file = new File("../riscv-simulator/src/test/resources/hk/quantr/riscv_simulator/xv6/kernel");
		final ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(file, 0, true);

		for (Dwarf dwarf : dwarfArrayList) {
			System.out.println(dwarf);
			for (CompileUnit cu : dwarf.compileUnits) {
				System.out.println(cu);
				for (DwarfLine line : cu.dwarfDebugLineHeader.lines) {
					System.out.println(cu.dwarfDebugLineHeader.filenames.get((int) line.file_num).file.getAbsolutePath() + "\t" + line);
				}
			}
		}
		long address = 0x80006090l;
		List<VariableWithAddress> variableWithAddresses = new ArrayList<VariableWithAddress>();

		CompileUnit cu = DwarfLib.getCompileUnit(dwarfArrayList, address);
		for (DwarfLine line : cu.dwarfDebugLineHeader.lines) {
			if (line.address.equals(BigInteger.valueOf(address))) {
				System.out.println(line.file_num + " = " + cu.dwarfDebugLineHeader.filenames.get((int) line.file_num).file.getAbsolutePath() + "\t" + line);
				VariableWithAddress variableWithAddress = new VariableWithAddress();
				variableWithAddress.file_num = line.file_num;
				variableWithAddress.line_num = line.line_num;
				variableWithAddress.address = line.address;
				variableWithAddresses.add(variableWithAddress);
			}
		}
	}
}

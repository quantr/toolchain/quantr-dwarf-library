
import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.dwarf.DwarfLine;
import hk.quantr.dwarf.helper.VariableWithAddress;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ParseRISCVPCLog {

	HashMap<Long, DwarfLine> cache = new HashMap<>();

	@Test
	public void test1() throws FileNotFoundException, IOException {
		File elf = new File(System.getProperty("user.home") + "/workspace/xv6-riscv/kernel/kernel");
		ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(elf, 0, false);

		File file = new File(System.getProperty("user.home") + "/workspace/xv6-riscv/pc2.log");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String l;
		long x = 0;
		while ((l = br.readLine()) != null) {
			long address = Long.parseLong(l.replaceAll(".*pc *", ""), 16);
			DwarfLine line = findLine(dwarfArrayList, address);

			if (line == null) {
				System.out.printf("%d : %x\n", x, address);
			} else {
				CompileUnit cu = DwarfLib.getCompileUnit(dwarfArrayList, address);
				System.out.printf("%d : %x %s:%d\n", x, address, cu.dwarfDebugLineHeader.filenames.get((int) line.file_num).file.getAbsolutePath(), line.line_num);
			}
			x++;
		}
		fr.close();
	}

	public DwarfLine findLine(ArrayList<Dwarf> dwarfArrayList, long address) {
		if (cache.get(address) != null) {
			return cache.get(address);
		}
		List<VariableWithAddress> variableWithAddresses = new ArrayList<VariableWithAddress>();
		CompileUnit cu = DwarfLib.getCompileUnit(dwarfArrayList, address);
		if (cu != null) {
			for (DwarfLine line : cu.dwarfDebugLineHeader.lines) {
				if (line.address.equals(BigInteger.valueOf(address))) {
					cache.put(address, line);
					return line;
				}
			}
		}
		return null;
	}
}


import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.DebugInfoEntry;
import hk.quantr.dwarf.dwarf.Dwarf;
import java.io.File;
import org.junit.Test;

public class Test1 {

	@Test
	public void test() {
		Dwarf dwarf = new Dwarf();
		dwarf.initElf(new File("src/test/resources/riscv/xv6/kernel"), "kernel.elf", 0, true);
//		System.out.println(dwarf.ehdr);

//		for (Elf_Phdr programHeader : dwarf.programHeaders) {
//			System.out.println(programHeader);
//		}
//		for (Elf_Shdr section : dwarf.sections) {
//			System.out.println(section);
		for (CompileUnit cu : dwarf.compileUnits) {
			System.out.println(cu);
			for (DebugInfoEntry entry : cu.debugInfoEntries) {
				System.out.println("\t" + entry);

				for (DebugInfoEntry entry2 : entry.debugInfoEntries) {
					System.out.println("\t\t" + entry2);
				}
			}
		}
	}
}

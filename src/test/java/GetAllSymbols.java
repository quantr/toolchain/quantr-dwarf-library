
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.elf.Elf64_Sym;
import hk.quantr.dwarf.elf.Elf_Sym;
import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import org.junit.Test;

public class GetAllSymbols {

	@Test
	public void test() {
		Dwarf dwarf = new Dwarf();
		dwarf.initElf(new File("../xv6-riscv/kernel/kernel".replaceFirst("^~", System.getProperty("user.home"))), "kernel.elf", 0, true);

		Collections.sort(dwarf.symbols, new Comparator<Elf_Sym>() {
			@Override
			public int compare(final Elf_Sym o1, final Elf_Sym o2) {
				return Long.valueOf(o1.getST_value()).compareTo(o2.getST_value());
			}
		});

		for (Elf_Sym symbol : dwarf.symbols) {
			if (symbol instanceof Elf64_Sym) {
				Elf64_Sym sym = (Elf64_Sym) symbol;
				System.out.println(sym.toString(dwarf.sections));
			} else {
				System.out.println(symbol);
			}
		}
	}
}

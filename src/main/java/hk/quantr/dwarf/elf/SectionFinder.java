package hk.quantr.dwarf.elf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.ArrayList;

public class SectionFinder {

	private static final byte ELFMAG0 = 0x7f;
	private static final byte ELFMAG1 = (byte) 'E';
	private static final byte ELFMAG2 = (byte) 'L';
	private static final byte ELFMAG3 = (byte) 'F';

	public static Elf_Shdr getSectionHeader(Elf_Ehdr ehdr, File file, String sectionName) throws IOException, FileNotFoundException {
		RandomAccessFile f = new RandomAccessFile(file, "r");

		/* Read the string table section header. */
		Elf_Shdr strtabhdr = null;
		if (SectionFinder.getElf32_Ehdr(file).is32Bits()) {
			strtabhdr = new Elf32_Shdr();
			f.seek(ehdr.getE_shoff().longValue() + (ehdr.getE_shstrndx() * Elf32_Shdr.sizeof()));
		} else if (SectionFinder.getElf32_Ehdr(file).is64Bits()) {
			strtabhdr = new Elf64_Shdr();
			f.seek(((BigInteger) ehdr.getE_shoff()).longValue() + (ehdr.getE_shstrndx() * Elf64_Shdr.sizeof()));
		}

		strtabhdr.read(ehdr.getE_shstrndx(), f);

		for (int i = 0; i < ehdr.getE_shnum(); i++) {
			Elf32_Shdr shdr = new Elf32_Shdr();
			// Read information about this section.
			if (SectionFinder.getElf32_Ehdr(file).is32Bits()) {
				f.seek(ehdr.getE_shoff().longValue() + (i * Elf32_Shdr.sizeof()));
			} else if (SectionFinder.getElf32_Ehdr(file).is64Bits()) {
				f.seek(((BigInteger) ehdr.getE_shoff()).longValue() + (i * Elf64_Shdr.sizeof()));
			}
			shdr.read(i, f);

			// Read the section name from the string table.
			f.seek(strtabhdr.getSh_offset().longValue() + shdr.sh_name);
			int bb;
			String sectionNameTemp = "";
			while ((bb = f.read()) > 0) {
				sectionNameTemp += (char) bb;
			}
			shdr.section_name = sectionNameTemp;
			if (sectionName.equals(sectionNameTemp)) {
				f.close();
				return shdr;
			}
		}
		f.close();
		return null;
	}

	public static Elf32_Ehdr getElf32_Ehdr(File file) throws IOException {
		Elf32_Ehdr ehdr = new Elf32_Ehdr();
		RandomAccessFile f = new RandomAccessFile(file, "r");
		ehdr.read(f);
		f.close();
		return ehdr;
	}

	public static ArrayList<Elf_Shdr> getAllSections(File file, int bits) throws IOException {
		ArrayList<Elf_Shdr> vector = new ArrayList<Elf_Shdr>();
		RandomAccessFile f = new RandomAccessFile(file, "r");

		/**
		 * Read the ELF header.
		 */
		if (bits == 32) {
			Elf32_Ehdr ehdr = new Elf32_Ehdr();
			ehdr.read(f);
			if (ehdr.e_ident[0] != ELFMAG0 || ehdr.e_ident[1] != ELFMAG1 || ehdr.e_ident[2] != ELFMAG2 || ehdr.e_ident[3] != ELFMAG3) {
				f.close();
				throw new IOException(file + ": not an ELF file");
			}

			/* Read the string table section header. */
			Elf32_Shdr strtabhdr = new Elf32_Shdr();
			f.seek(ehdr.e_shoff.longValue() + (ehdr.e_shstrndx * Elf32_Shdr.sizeof()));
			strtabhdr.read(ehdr.e_shstrndx, f);

			for (int i = 0; i < ehdr.e_shnum; i++) {
				Elf32_Shdr shdr = new Elf32_Shdr();
				// Read information about this section.
				f.seek(ehdr.e_shoff.longValue() + (i * Elf32_Shdr.sizeof()));
				shdr.read(i, f);

				// Read the section name from the string table.
				f.seek(strtabhdr.sh_offset.longValue() + shdr.sh_name);
				int bb;
				String sectionName = "";
				while ((bb = f.read()) > 0) {
					sectionName += (char) bb;
				}
				shdr.section_name = sectionName;
				vector.add(shdr);
			}
			f.close();
		} else if (bits == 64) {
			Elf64_Ehdr ehdr = new Elf64_Ehdr();
			ehdr.read(f);
			if (ehdr.e_ident[0] != ELFMAG0 || ehdr.e_ident[1] != ELFMAG1 || ehdr.e_ident[2] != ELFMAG2 || ehdr.e_ident[3] != ELFMAG3) {
				f.close();
				throw new IOException(file + ": not an ELF file");
			}

			/* Read the string table section header. */
			Elf64_Shdr strtabhdr = new Elf64_Shdr();
			f.seek(ehdr.e_shoff.longValue() + (ehdr.e_shstrndx * Elf64_Shdr.sizeof()));
			strtabhdr.read(ehdr.e_shstrndx, f);

			for (int i = 0; i < ehdr.e_shnum; i++) {
				Elf64_Shdr shdr = new Elf64_Shdr();
				// Read information about this section.
				f.seek(ehdr.e_shoff.longValue() + (i * Elf64_Shdr.sizeof()));
				shdr.read(i, f);

				// Read the section name from the string table.
				f.seek(strtabhdr.sh_offset.longValue() + shdr.sh_name);
				int bb;
				String sectionName = "";
				while ((bb = f.read()) > 0) {
					sectionName += (char) bb;
				}
				shdr.section_name = sectionName;
				vector.add(shdr);
			}
			f.close();
		}
		return vector;
	}

	public static Elf_Shdr getSection(File file, String sectionName, int bits) {
		try {
			for (Elf_Shdr s : getAllSections(file, bits)) {
				if (s.getSection_name().equals(sectionName)) {
					return s;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ArrayList<Elf_Shdr> getAllRelocationSection(File file, int bits) {
		ArrayList<Elf_Shdr> temp = new ArrayList<Elf_Shdr>();
		ArrayList<Elf_Shdr> sections = new ArrayList<Elf_Shdr>();
		try {
			temp = getAllSections(file, bits);
			for (Elf_Shdr s : temp) {
				if (s.getSh_type() == 4 || s.getSh_type() == 9) {
					sections.add(s);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sections;
	}

	public static ByteBuffer findSectionByte(Elf_Ehdr ehdr, File file, String section) throws IOException {
		RandomAccessFile f = new RandomAccessFile(file, "r");

		/* Read the string table section header. */
		Elf_Shdr strtabhdr = null;
		if (SectionFinder.getElf32_Ehdr(file).is32Bits()) {
			strtabhdr = new Elf32_Shdr();
			f.seek(((BigInteger) ehdr.getE_shoff()).longValue() + (ehdr.getE_shstrndx() * Elf32_Shdr.sizeof()));
		} else if (SectionFinder.getElf32_Ehdr(file).is64Bits()) {
			strtabhdr = new Elf64_Shdr();
			f.seek(((BigInteger) ehdr.getE_shoff()).longValue() + (ehdr.getE_shstrndx() * Elf64_Shdr.sizeof()));
		}

		strtabhdr.read(ehdr.getE_shstrndx(), f);

		Elf_Shdr shdr = null;
		if (SectionFinder.getElf32_Ehdr(file).is32Bits()) {
			shdr = new Elf32_Shdr();
		} else if (SectionFinder.getElf32_Ehdr(file).is64Bits()) {
			shdr = new Elf64_Shdr();
		}

		byte[] target_bytes = section.getBytes("ISO8859-1");
		byte[] buf = new byte[target_bytes.length + 1];
		boolean found = false;

		outer:
		for (int i = 0; i < ehdr.getE_shnum(); i++) {
			// Read information about this section.
			f.seek(((BigInteger) ehdr.getE_shoff()).longValue() + (i * Elf64_Shdr.sizeof()));
			shdr.read(i, f);

			// Read the section name from the string table.
			f.seek(strtabhdr.getSh_offset().longValue() + shdr.getSh_name());
			f.readFully(buf);

			if (!Arrays.equals(target_bytes, Arrays.copyOf(buf, target_bytes.length))) {
				continue outer;
			}
			if (buf[buf.length - 1] != '\0') {
				continue;
			}
			found = true;
			break;
		}

		if (!found) {
			f.close();
			System.err.println("no section " + section + " found in " + file);
			return null;
		}

		try {
			byte bytes[] = new byte[shdr.getSh_size().intValue()];
			f.seek(shdr.getSh_offset().longValue());
			f.readFully(bytes);
			f.close();
			ByteBuffer buffer = ByteBuffer.wrap(bytes);
			buffer.order(ByteOrder.nativeOrder());
			return buffer;
		} finally {
			f.close();
		}
	}

}

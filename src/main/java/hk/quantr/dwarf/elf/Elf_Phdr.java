package hk.quantr.dwarf.elf;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;

public interface Elf_Phdr {

	public int sizeof();

	public void read(RandomAccessFile f) throws IOException;

	public int getP_type();

	public BigInteger getP_offset();

	public BigInteger getP_vaddr();

	public BigInteger getP_paddr();

	public BigInteger getP_filesz();

	public BigInteger getP_memsz();

	public int getP_flags();

	public BigInteger getP_align();

}

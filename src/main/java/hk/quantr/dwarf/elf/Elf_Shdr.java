package hk.quantr.dwarf.elf;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public interface Elf_Shdr {

	int getNumber();

	String getSection_name();

	BigInteger getSh_addr();

	BigInteger getSh_addralign();

	BigInteger getSh_entsize();

	BigInteger getSh_flags();

	int getSh_info();

	int getSh_link();

	long getSh_name();

	BigInteger getSh_offset();

	BigInteger getSh_size();

	int getSh_type();

	void read(int number, RandomAccessFile f) throws IOException;

}

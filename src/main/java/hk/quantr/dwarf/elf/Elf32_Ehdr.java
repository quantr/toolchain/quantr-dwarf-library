package hk.quantr.dwarf.elf;

import java.io.IOException;
import java.io.RandomAccessFile;
import hk.quantr.dwarf.dwarf.DwarfLib;
import java.math.BigInteger;

public class Elf32_Ehdr implements Elf_Ehdr {

	public static final int EI_NIDENT = 16;

	public byte[] e_ident = new byte[EI_NIDENT];/* Magic number and other info. */
	public int e_type;
	/* Object file type. */
	public int e_machine;
	/* Architecture type. */
	public int e_version;
	/* Object file version. */
	public BigInteger e_entry;
	/* Entry point virtual address. */
	public BigInteger e_phoff;
	/* Program header table file offset. */
	public BigInteger e_shoff;
	/* Section header table file offset. */
	public int e_flags;
	/* Processor-specific flags. */
	public int e_ehsize;
	/* ELF header size in bytes. */
	public int e_phentsize;
	/* Program header table entry size. */
	public int e_phentnum;
	/* Program header table entry count. */
	public int e_shentsize;
	/* Section header table entry size. */
	public int e_shnum;
	/* Section header table entry count. */
	public int e_shstrndx;

	/* Section header string table index. */
	public Elf32_Ehdr() {
	}

	public void read(RandomAccessFile f) throws IOException {
		f.readFully(e_ident);
		e_type = DwarfLib.readUHalf(f);
		e_machine = DwarfLib.readUHalf(f);
		e_version = DwarfLib.readWord(f);
		e_entry = BigInteger.valueOf(DwarfLib.readUWord(f));
		e_phoff = BigInteger.valueOf(DwarfLib.readUWord(f));
		e_shoff = BigInteger.valueOf(DwarfLib.readUWord(f));
		e_flags = DwarfLib.readWord(f);
		e_ehsize = DwarfLib.readUHalf(f);
		e_phentsize = DwarfLib.readUHalf(f);
		e_phentnum = DwarfLib.readUHalf(f);
		e_shentsize = DwarfLib.readUHalf(f);
		e_shnum = DwarfLib.readUHalf(f);
		e_shstrndx = DwarfLib.readUHalf(f);
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("[ e_ident: ");
		for (int i = 0; i < EI_NIDENT; i++) {
			if (e_ident[i] < 0x10 && e_ident[i] >= 0) {
				str.append('0');
			}
			str.append(Integer.toHexString(e_ident[i] & 0xFF));
		}
		str.append("; e_type: ").append(e_type & 0xFFFF);
		str.append("; e_machine: ").append(e_machine & 0xFFFF);
		str.append("; e_version: ").append(e_version & 0xFFFF);
		str.append("; e_entry: 0x").append(e_entry.toString(16));
		str.append("; e_phoff: ").append((long) e_phoff.longValue() & 0xFFFFFFFFL);
		str.append("; e_shoff: ").append((long) e_shoff.longValue() & 0xFFFFFFFFL);
		str.append("; e_flags: 0x").append(Integer.toHexString(e_flags));
		str.append("; e_ehsize: ").append(e_ehsize & 0xFFFF);
		str.append("; e_phentsize: ").append(e_phentsize & 0xFFFF);
		str.append("; e_phentnum: ").append(e_phentnum & 0xFFFF);
		str.append("; e_shentsize: ").append(e_shentsize & 0xFFFF);
		str.append("; e_shnum: ").append(e_shnum & 0xFFFF);
		str.append("; e_shstrndx: ").append(e_shstrndx & 0xFFFF);
		str.append(" ]");
		return str.toString();
	}

	public boolean is32Bits() {
		return e_ident[4] == 1;
	}

	public boolean is64Bits() {
		return e_ident[4] == 2;
	}

	public int getEI_NIDENT() {
		return EI_NIDENT;
	}

	public byte[] getE_ident() {
		return e_ident;
	}

	public int getE_type() {
		return e_type;
	}

	public int getE_machine() {
		return e_machine;
	}

	public int getE_version() {
		return e_version;
	}

	public BigInteger getE_entry() {
		return e_entry;
	}

	public BigInteger getE_phoff() {
		return e_phoff;
	}

	public BigInteger getE_shoff() {
		return e_shoff;
	}

	public int getE_flags() {
		return e_flags;
	}

	public int getE_ehsize() {
		return e_ehsize;
	}

	public int getE_phentsize() {
		return e_phentsize;
	}

	public int getE_phentnum() {
		return e_phentnum;
	}

	public int getE_shentsize() {
		return e_shentsize;
	}

	public int getE_shnum() {
		return e_shnum;
	}

	public int getE_shstrndx() {
		return e_shstrndx;
	}
}

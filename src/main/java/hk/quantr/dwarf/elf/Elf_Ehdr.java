package hk.quantr.dwarf.elf;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;

public interface Elf_Ehdr {

	public void read(RandomAccessFile f) throws IOException;


	public int getEI_NIDENT();

	public byte[] getE_ident();

	public int getE_type();

	public int getE_machine();

	public int getE_version();

	public BigInteger getE_entry();

	public BigInteger getE_phoff();

	public BigInteger getE_shoff();

	public int getE_flags();

	public int getE_ehsize();

	public int getE_phentsize();

	public int getE_phentnum();

	public int getE_shentsize();

	public int getE_shnum();

	public int getE_shstrndx();

}

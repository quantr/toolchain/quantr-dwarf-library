package hk.quantr.dwarf.elf;

public class Elf32_Sym implements Elf_Sym {

	public int st_name;
	public int st_value;
	public int st_size;
	public byte st_info;
	public byte st_other;
	public short st_shndx;
	public String name;

	@Override
	public int compareTo(Elf_Sym o) {
		if (st_value == o.getST_value()) {
			return 0;
		} else if (name != null && o.getName() != null) {
			return name.compareToIgnoreCase(o.getName());
		} else {
			return 0;
		}
	}

	public boolean checkWithinRange(long address) {
		if (address >= st_value && address <= st_value + st_size) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return String.format("%08x\t%5d\t%5d\t%s", st_value, st_size, st_info, name);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public long getST_value() {
		return st_value;
	}
}

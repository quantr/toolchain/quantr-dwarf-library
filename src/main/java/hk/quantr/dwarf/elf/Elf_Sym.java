package hk.quantr.dwarf.elf;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public interface Elf_Sym extends Comparable<Elf_Sym>{

	public long getST_value();

	public String getName();
}

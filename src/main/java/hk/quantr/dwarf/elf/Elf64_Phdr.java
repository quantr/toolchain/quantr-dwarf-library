package hk.quantr.dwarf.elf;

import hk.quantr.dwarf.dwarf.DwarfLib;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;

public class Elf64_Phdr implements Elf_Phdr {

	public int p_type;
	/* Identifies program segment type */
	public BigInteger p_offset;
	/* Segment file offset */
	public BigInteger p_vaddr;
	/* Segment virtual address */
	public BigInteger p_paddr;
	/* Segment physical address */
	public BigInteger p_filesz;
	/* Segment size in file */
	public BigInteger p_memsz;
	/* Segment size in memory */
	public int p_flags;
	/* Segment flags */
	public BigInteger p_align;

	/* Segment alignment, file & memory */
	public int sizeof() {
		return 32;
	}

	public void read(RandomAccessFile f) throws IOException {
		p_type = DwarfLib.readWord(f);
		p_flags = DwarfLib.readWord(f);
		p_offset = DwarfLib.readU64Bits(f);
		p_vaddr = DwarfLib.readU64Bits(f);
		p_paddr = DwarfLib.readU64Bits(f);
		p_filesz = DwarfLib.readU64Bits(f);
		p_memsz = DwarfLib.readU64Bits(f);
		p_align = DwarfLib.readU64Bits(f);
	}

	@Override
	public String toString() {
		// StringBuffer str = new StringBuffer(super.toString());
		StringBuffer str = new StringBuffer(super.toString());
		str.append("[ p_type: ").append(p_type);
		str.append("; p_offset: 0x").append(p_offset.toString(16));
		str.append("; p_vaddr: 0x").append(p_vaddr.toString(16));
		str.append("; p_paddr: 0x").append(p_paddr.toString(16));
		str.append("; p_filesz: 0x").append(p_filesz.toString(16));
		str.append("; p_memsz: 0x").append(p_memsz.toString(16));
		str.append("; p_flags: 0x").append(Integer.toHexString(p_flags));
		str.append("; p_align: 0x").append(p_align.toString(16));
		str.append(" ]");
		return str.toString();
	}

	public int getP_type() {
		return p_type;
	}

	public BigInteger getP_offset() {
		return p_offset;
	}

	public BigInteger getP_vaddr() {
		return p_vaddr;
	}

	public BigInteger getP_paddr() {
		return p_paddr;
	}

	public BigInteger getP_filesz() {
		return p_filesz;
	}

	public BigInteger getP_memsz() {
		return p_memsz;
	}

	public int getP_flags() {
		return p_flags;
	}

	public BigInteger getP_align() {
		return p_align;
	}

}

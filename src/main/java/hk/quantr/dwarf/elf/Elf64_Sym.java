package hk.quantr.dwarf.elf;

import java.util.ArrayList;

public class Elf64_Sym implements Elf_Sym {

	public int st_name;
	public long st_value;
	public long st_size;
	public byte st_info;
	public byte st_other;
	public short st_shndx;
	public String name;

	@Override
	public int compareTo(Elf_Sym o) {
		if (st_value == o.getST_value()) {
			return 0;
		} else if (name != null && o.getName() != null) {
			return name.compareToIgnoreCase(o.getName());
		} else {
			return 0;
		}
	}

	public boolean checkWithinRange(long address) {
		if (address >= st_value && address <= st_value + st_size) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return String.format("%08x\t%d\t%s\t%s\t%s\t%s\t%s", st_value, st_size,
				Elf_Common.getSTTypeName(Elf_Common.ELF32_ST_TYPE(st_info)),
				Elf_Common.getSTBindName(Elf_Common.ELF32_ST_BIND(st_info)),
				Elf_Common.get_symbol_visibility(Elf_Common.ELF_ST_VISIBILITY(st_other)),
				Elf_Common.get_symbol_index_type(st_shndx),
				name);
	}

	public String toString(ArrayList<Elf_Shdr> sections) {
		if (Elf_Common.ELF32_ST_TYPE(st_info) == Elf_Common.STT_SECTION) {
			return String.format("%08x\t%d\t%s\t%s\t%s\t%s\t%s", st_value, st_size,
					Elf_Common.getSTTypeName(Elf_Common.ELF32_ST_TYPE(st_info)),
					Elf_Common.getSTBindName(Elf_Common.ELF32_ST_BIND(st_info)),
					Elf_Common.get_symbol_visibility(Elf_Common.ELF_ST_VISIBILITY(st_other)),
					Elf_Common.get_symbol_index_type(st_shndx),
					sections.get(st_shndx).getSection_name());
		} else {
			return toString();
		}
	}

	@Override
	public long getST_value() {
		return st_value;
	}

	@Override
	public String getName() {
		return name;
	}

}

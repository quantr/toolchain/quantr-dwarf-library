package hk.quantr.dwarf.elf;

import java.io.IOException;
import java.io.RandomAccessFile;

import hk.quantr.dwarf.dwarf.DwarfLib;
import java.math.BigInteger;

public class Elf32_Shdr implements Elf_Shdr {

	public int number;
	public long sh_name;
	public String section_name;
	public int sh_type;
	public BigInteger sh_flags;
	public BigInteger sh_addr;
	public BigInteger sh_offset;
	public BigInteger sh_size;
	public int sh_link;
	public int sh_info;
	public BigInteger sh_addralign;
	public BigInteger sh_entsize;

	public Elf32_Shdr() {
	}

	public static int sizeof() {
		return 40;
	}

	public void read(int number, RandomAccessFile f) throws IOException {
		this.number = number;
		sh_name = DwarfLib.readUWord(f);
		sh_type = DwarfLib.readWord(f);
		sh_flags = BigInteger.valueOf(DwarfLib.readWord(f));
		sh_addr = BigInteger.valueOf(DwarfLib.readUWord(f));
		sh_offset = BigInteger.valueOf(DwarfLib.readUWord(f));
		sh_size = BigInteger.valueOf(DwarfLib.readUWord(f));
		sh_link = DwarfLib.readWord(f);
		sh_info = DwarfLib.readWord(f);
		sh_addralign = BigInteger.valueOf(DwarfLib.readWord(f));
		sh_entsize = BigInteger.valueOf(DwarfLib.readWord(f));
	}

	@Override
	public String toString() {
		StringBuffer str = new StringBuffer(super.toString());
//		StringBuffer str = new StringBuffer();
		str.append("[ number: ").append(number);
		str.append("; sh_name: ").append(sh_name);
		str.append("; section_name: ").append(section_name);
		str.append("; sh_type: ").append(sh_type);
		str.append("; sh_flags: 0x").append(sh_flags.toString(16));
		str.append("; sh_addr: 0x").append(sh_addr.toString(16));
		str.append("; sh_offset: ").append(sh_offset);
		str.append("; sh_size: ").append(sh_size);
		str.append("; sh_link: ").append(sh_link);
		str.append("; sh_info: ").append(sh_info);
		str.append("; sh_addralgin: ").append(sh_addralign);
		str.append("; sh_entsize: ").append(sh_entsize);
		str.append(" ]");
		return str.toString();
	}

	public int getNumber() {
		return number;
	}

	public long getSh_name() {
		return sh_name;
	}

	public String getSection_name() {
		return section_name;
	}

	public int getSh_type() {
		return sh_type;
	}

	public BigInteger getSh_flags() {
		return sh_flags;
	}

	public BigInteger getSh_addr() {
		return sh_addr;
	}

	public BigInteger getSh_offset() {
		return sh_offset;
	}

	public BigInteger getSh_size() {
		return sh_size;
	}

	public int getSh_link() {
		return sh_link;
	}

	public int getSh_info() {
		return sh_info;
	}

	public BigInteger getSh_addralign() {
		return sh_addralign;
	}

	public BigInteger getSh_entsize() {
		return sh_entsize;
	}

}

package hk.quantr.dwarf.helper;

import java.io.File;
import java.math.BigInteger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VariableWithAddress implements Comparable {

	public File file;
	public long file_num;
	public int line_num;
	public BigInteger address;

	public String variableName;
	public String type;

	public VariableWithAddress() {

	}

	public VariableWithAddress(File file, long file_num, int line_num, BigInteger address) {
		this.file = file;
		this.file_num = file_num;
		this.line_num = line_num;
		this.address = address;
	}

	@Override
	public int compareTo(Object o) {
		if (o instanceof VariableWithAddress) {
			VariableWithAddress o2 = (VariableWithAddress) o;
			return address.compareTo(o2.address);
		} else {
			throw new UnsupportedOperationException("Not supported yet.");
		}
	}
}

package hk.quantr.dwarf;

import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.DebugInfoAbbrevEntry;
import hk.quantr.dwarf.dwarf.DebugInfoEntry;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import java.io.File;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author peter
 */
public class TestPeterDwarf_GetFunctions {

	public static void main(String[] args) {
		if (args.length == 0) {
			QuantrDwarf.println("java -jar peter-dwarf.jar <your elf file path>");
			System.exit(6);
		}

		for (String str : args) {
			if (str.contains("-debug")) {
				DwarfGlobal.debug = true;
			}
		}

		Dwarf dwarf = new Dwarf();
		File file = new File(args[0]);

		if (file.isDirectory()) {
			return;
		}

		ArrayList<Dwarf> dwarfLib = DwarfLib.init(file, 0, true);
		if (dwarfLib == null) {
			System.err.println("dwarf init fail");
			//$hide>>$
			System.exit(1);
			//$hide<<$
		} else if (DwarfGlobal.debug) {
			QuantrDwarf.println(".debug_info:");
			for (CompileUnit compileUnit : dwarf.compileUnits) {
				QuantrDwarf.printf("Compilation Unit @ offset 0x%x\n", compileUnit.offset);
				QuantrDwarf.printf("Length: 0x%x\n", compileUnit.length);
				QuantrDwarf.println("Version: " + compileUnit.version);
				QuantrDwarf.printf("Abbrev Offset: 0x%x\n", compileUnit.offset);
				QuantrDwarf.println("Pointer Size: " + compileUnit.addr_size);

				for (DebugInfoEntry debugInfoEntry : compileUnit.debugInfoEntries) {
					QuantrDwarf.println("<" + debugInfoEntry.position + "> Abbrev Number: " + debugInfoEntry.abbrevNo + " (" + debugInfoEntry.name + ")");

					Enumeration<String> e = Collections.enumeration(debugInfoEntry.debugInfoAbbrevEntries.keySet());
					while (e.hasMoreElements()) {
						String key = e.nextElement();
						DebugInfoAbbrevEntry debugInfoAbbrevEntry = debugInfoEntry.debugInfoAbbrevEntries.get(key);
						if (debugInfoAbbrevEntry.value == null) {
							QuantrDwarf.printf("<%x>\t%s\tnull\n", debugInfoAbbrevEntry.position, debugInfoAbbrevEntry.name);
						} else if (debugInfoAbbrevEntry.value instanceof String) {
							QuantrDwarf.printf("<%x>\t%s\t%s\n", debugInfoAbbrevEntry.position, debugInfoAbbrevEntry.name, debugInfoAbbrevEntry.value);
						} else if (debugInfoAbbrevEntry.value instanceof Byte || debugInfoAbbrevEntry.value instanceof Integer || debugInfoAbbrevEntry.value instanceof Long) {
							QuantrDwarf.printf("<%x>\t%s\t%x\n", debugInfoAbbrevEntry.position, debugInfoAbbrevEntry.name, debugInfoAbbrevEntry.value);
						} else if (debugInfoAbbrevEntry.value instanceof byte[]) {
							byte[] bytes = (byte[]) debugInfoAbbrevEntry.value;
							QuantrDwarf.printf("<%x>\t%s\t", debugInfoAbbrevEntry.position, debugInfoAbbrevEntry.name);
							for (byte b : bytes) {
								QuantrDwarf.printf("%x ", b);
							}
							QuantrDwarf.println();
						} else {
							QuantrDwarf.println("not support value format : " + debugInfoAbbrevEntry.value.getClass().toString());
						}
					}
				}
			}

			QuantrDwarf.println();

//			for (DwarfDebugLineHeader header : dwarf.headers) {
//				QuantrDwarf.log();
//				QuantrDwarf.log(header);
//				QuantrDwarf.log();
//
//				QuantrDwarf.log("dirnames:");
//				for (String s : header.dirnames) {
//					QuantrDwarf.log(s);
//				}
//				QuantrDwarf.log();
//
//				QuantrDwarf.log("entry\tdir\ttime\tlen\tfilename");
//				for (DwarfHeaderFilename filename : header.filenames) {
//					QuantrDwarf.log(filename.entryNo + "\t" + filename.dir + "\t" + filename.time + "\t" + filename.len + "\t" + filename.file.getAbsolutePath());
//				}
//				QuantrDwarf.log();
//
//				QuantrDwarf.log("address\tfile no.\tline no.\tcolumn no.\taddress");
//
//				for (DwarfLine line : header.lines) {
//					QuantrDwarf.log("\t" + line.file_num + "\t\t" + line.line_num + "\t\t" + line.column_num + "\t\t" + line.address.toString(16));
//				}
//				QuantrDwarf.log();
//				QuantrDwarf.log();
//			}
		}
		// DwarfLib.printMappedByteBuffer(dwarf.byteBuffer);

		// dwarf.printHeader();
	}
}

package hk.quantr.dwarf;

import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfDebugLineHeader;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.dwarf.DwarfLine;
import hk.quantr.dwarf.elf.Elf64_Sym;
import hk.quantr.dwarf.elf.Elf_Sym;
import hk.quantr.dwarf.helper.VariableWithAddress;
import hk.quantr.javalib.CommonLib;
import java.io.File;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;

public class QuantrDwarf {

	public static boolean isDebug = true;

	public static void main(String[] args) {
		if (args.length > 1) {
			String file = args[0].replaceFirst("^~", System.getProperty("user.home"));
			String command = args[1].toLowerCase().trim();

			final ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(new File(file), 0, false);
			isDebug = true;
			if (command.equals("findfunction")) {
				long address = CommonLib.string2long(args[2]);
				QuantrDwarf.println("function = " + DwarfLib.getFunctionName(dwarfArrayList, address));
			} else if (command.equals("findparameter")) {
				long address = CommonLib.string2long(args[2]);
				QuantrDwarf.println("function = " + DwarfLib.getParameters(dwarfArrayList, address));
			} else if (command.equals("functions")) {
				for (String name : DwarfLib.getAllFunctionNames(dwarfArrayList)) {
					QuantrDwarf.println(name);
				}
			} else if (command.equals("findccode")) {
				long address = CommonLib.string2long(args[2]);
				ArrayList<String> s;
				int preventInfiniteLoop = 0;
				do {
					s = getCCode(dwarfArrayList, BigInteger.valueOf(address), true);
					address++;
					preventInfiniteLoop++;
				} while (s == null && preventInfiniteLoop < 10);
				if (s != null && !s.isEmpty()) {
					for (String temp : s) {
						QuantrDwarf.println(temp);
					}
				}
			} else if (command.equals("findaddress")) {
				String filename = args[2];
				long lineNo = CommonLib.string2long(args[3]);
				BigInteger address = DwarfLib.getAddress(dwarfArrayList, filename, lineNo);
				QuantrDwarf.println(address);
			} else if (command.equals("listcompileunit")) {
				for (Dwarf dwarf : dwarfArrayList) {
					for (CompileUnit cu : dwarf.compileUnits) {
						System.out.println(cu);
					}
				}
			} else if (command.equals("symbol")) {
				for (Dwarf dwarf : dwarfArrayList) {
					for (Elf_Sym symbol : dwarf.symbols) {
						if (symbol instanceof Elf64_Sym) {
							Elf64_Sym sym = (Elf64_Sym) symbol;
							System.out.println(sym.toString(dwarf.sections));
						} else {
							System.out.println(symbol);
						}
					}
				}
			} else {
				QuantrDwarf.println("Command not found: " + command);
			}
		} else if (args[0].equals("version")) {
			QuantrDwarf.println("version = " + PropertyUtil.getProperty("version"));
		} else {
			QuantrDwarf.println("Command argument not found");
		}
	}

	public static void println(Object obj) {
		if (isDebug) {
			System.out.println(obj);
		}
	}

	public static void println() {
		if (isDebug) {
			System.out.println();
		}
	}

	public static void print(Object obj) {
		if (isDebug) {
			System.out.print(obj);
		}
	}

	public static void printf(String format, Object... args) {
		if (isDebug) {
			System.out.printf(format, args);
		}
	}

	public static ArrayList<String> getCCode(ArrayList<Dwarf> dwarfArrayList, BigInteger pc, boolean getFile) {
		HashMap<File, List<String>> fileCaches = new HashMap<>();
		for (Dwarf dwarf : dwarfArrayList) {
			try {
				DwarfLine startLine = null;
				DwarfLine endLine = null;
				DwarfDebugLineHeader startHeader = null;
				loop1:
				for (CompileUnit cu : dwarf.compileUnits) {
					DwarfDebugLineHeader header = cu.dwarfDebugLineHeader;
					boolean toggle = false;
					for (DwarfLine line : header.lines) {
//						if (line.address.compareTo(BigInteger.valueOf(0x1606b3e)) == 1 && line.address.compareTo(BigInteger.valueOf(0x1606c3e))==-1) {
//							QuantrDwarf.log("line.address=%x\n", line.address);
//						}

						if (!toggle && line.address.equals(pc)) {
							startLine = line;
							startHeader = header;
							toggle = true;
							continue;
						}
						if (toggle && !line.address.equals(startLine.address) && line.line_num != startLine.line_num) {
							endLine = line;
							break loop1;
						}
					}
					startLine = null;
					endLine = null;
				}
				if (startHeader == null || startLine == null) {
					return null;
				}

				File file = startHeader.filenames.get((int) startLine.file_num).file;
				List<String> sourceLines = fileCaches.get(file);
				if (sourceLines == null && file.exists()) {
					sourceLines = FileUtils.readLines(file);
					fileCaches.put(file, sourceLines);
				}

				int endLineNo = 0;
				if (endLine == null) {
					endLineNo = sourceLines.size() - startLine.line_num;
				} else {
					endLineNo = endLine.line_num - 1;
				}
				if (sourceLines != null) {
					ArrayList<String> s = new ArrayList<>();
					for (int z = startLine.line_num - 1, index = 0; z < endLineNo && z < sourceLines.size(); z++, index++) {
						if (getFile) {
							s.add(startHeader.filenames.get((int) startLine.file_num).file + " : " + (z + 1));
						} else {
							String cCode = startHeader.filenames.get((int) startLine.file_num).file + " : " + (z + 1) + "\t" + sourceLines.get(z);
							s.add(cCode);
						}
					}
					return s;
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public static List<VariableWithAddress> getFilePathAndLineNos(ArrayList<Dwarf> dwarfArrayList, BigInteger address) {
		List<VariableWithAddress> variableWithAddresses = new ArrayList<VariableWithAddress>();

		CompileUnit cu = DwarfLib.getCompileUnit(dwarfArrayList, address.longValue());
		if (cu == null) {
			return null;
		}
		for (DwarfLine line : cu.dwarfDebugLineHeader.lines) {
			if (line.address.compareTo(address) < 0) {
				continue;
			}
			VariableWithAddress variableWithAddress = new VariableWithAddress();
			variableWithAddress.file_num = line.file_num;
			variableWithAddress.line_num = line.line_num;
			variableWithAddress.address = line.address;
			variableWithAddresses.add(variableWithAddress);
		}
		return variableWithAddresses;
	}

	public static VariableWithAddress getFilePathAndLineNo(ArrayList<Dwarf> dwarfArrayList, BigInteger address) {
		if (dwarfArrayList == null) {
			return null;
		}
		CompileUnit cu = DwarfLib.getCompileUnit(dwarfArrayList, address.longValue());
		if (cu != null) {
			for (int x = cu.dwarfDebugLineHeader.lines.size() - 1; x >= 0; x--) {
				DwarfLine line = cu.dwarfDebugLineHeader.lines.get(x);
				if (line.address.compareTo(address) > 0) {
					continue;
				}
				VariableWithAddress variableWithAddress = new VariableWithAddress();
				variableWithAddress.file = cu.dwarfDebugLineHeader.filenames.get((int) line.file_num).file;
				variableWithAddress.file_num = line.file_num;
				variableWithAddress.line_num = line.line_num;
				variableWithAddress.address = line.address;
				return variableWithAddress;
			}
		}

		for (Dwarf dwarf : dwarfArrayList) {
			Collections.sort(dwarf.symbols, new Comparator<Elf_Sym>() {
				@Override
				public int compare(final Elf_Sym o1, final Elf_Sym o2) {
					return Long.valueOf(o1.getST_value()).compareTo(o2.getST_value());
				}
			});

			Elf_Sym lastSymbol = null;
			for (Elf_Sym symbol : dwarf.symbols) {
				if (symbol.getST_value() != 0 && address.longValue() < symbol.getST_value()) {
					VariableWithAddress v = new VariableWithAddress();
					v.file = new File(lastSymbol.getName());
					return v;
				}
				lastSymbol = symbol;
			}
		}
		return null;
	}
}

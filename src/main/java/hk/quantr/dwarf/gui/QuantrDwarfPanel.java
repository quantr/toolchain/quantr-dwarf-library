package hk.quantr.dwarf.gui;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import hk.quantr.dwarf.dwarf.Abbrev;
import hk.quantr.dwarf.dwarf.AbbrevEntry;
import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.DebugInfoAbbrevEntry;
import hk.quantr.dwarf.dwarf.DebugInfoEntry;
import hk.quantr.dwarf.dwarf.DebugLocEntry;
import hk.quantr.dwarf.dwarf.Definition;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfDebugLineHeader;
import hk.quantr.dwarf.dwarf.DwarfHeaderFilename;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.dwarf.DwarfLine;
import hk.quantr.dwarf.dwarf.FrameChunk;
import hk.quantr.dwarf.elf.Elf_Shdr;
import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.swing.FilterTreeModel;
import hk.quantr.javalib.swing.advancedswing.jprogressbardialog.JProgressBarDialog;
import hk.quantr.javalib.swing.advancedswing.searchtextfield.JSearchTextField;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ToolTipManager;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

public class QuantrDwarfPanel extends JPanel {

	DwarfTreeCellRenderer treeCellRenderer;
	DwarfTreeNode root;
	DefaultTreeModel treeModel;
	FilterTreeModel filterTreeModel;
	JTree tree;
	ArrayList<File> files;
	public ArrayList<Dwarf> dwarfs;
	boolean showDialog;
	JSearchTextField searchTextField;

	final int maxExpandLevel = 5;
	final int maxPoolSize = 8;

	ExecutorService pool;
	JProgressBar progressBar = new JProgressBar();
	private boolean showDebugLoc;
	private boolean showDebugInfoEntriesInCompileUnit;

	public QuantrDwarfPanel() {
		this.searchTextField = new JSearchTextField();
		this.treeCellRenderer = new DwarfTreeCellRenderer();
		this.root = new DwarfTreeNode("Elf files", null, null);
		this.treeModel = new DefaultTreeModel(root);
		this.filterTreeModel = new FilterTreeModel(treeModel, 10, true);
		this.tree = new JTree();
		tree.setModel(filterTreeModel);
		this.dwarfs = new ArrayList<>();
		this.files = new ArrayList<>();
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		tree.setShowsRootHandles(true);
		tree.setCellRenderer(treeCellRenderer);
		scrollPane.setViewportView(tree);

		JToolBar toolBar = new JToolBar();
		add(toolBar, BorderLayout.NORTH);

		JButton expandAllButton = new JButton("expand");
		expandAllButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CommonLib.expandAll(tree, true, maxExpandLevel);
			}
		});
		toolBar.add(expandAllButton);
		JButton collapseButton = new JButton("collapse");
		collapseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				filterTreeModel.reload();
				CommonLib.expandAll(tree, false);
			}
		});
		toolBar.add(collapseButton);

		searchTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e != null && e.getKeyCode() == 10) {
					//int nodeCount = CommonLib.getNumberOfNodes(filterTreeModel);
					//QuantrDwarf.log("getNumberOfNodes=" + nodeCount);

					progressBar.setVisible(false);
					searchTextField.setEditable(false);
					filterTreeModel.filter = searchTextField.getText();
					filterTreeModel.reload();

					if (searchTextField.getText().equals("")) {
						expandFirstLevel();
					} else {
						CommonLib.expandAll(tree, true, maxExpandLevel);
					}
					searchTextField.setEditable(true);
					progressBar.setVisible(false);
				}
			}
		});
		searchTextField.setMinimumSize(new Dimension(200, 20));
		searchTextField.setMaximumSize(new Dimension(200, 20));
		searchTextField.setPreferredSize(new Dimension(200, 20));
		toolBar.add(searchTextField);

		progressBar.setStringPainted(true);
		progressBar.setVisible(false);
		toolBar.add(progressBar);
		ToolTipManager.sharedInstance().registerComponent(tree);
	}

	public void clear() {
		root.children.clear();
		treeModel.nodeStructureChanged(root);
	}

	public void init(final File file, long memoryOffset) {
		//		init(file, memoryOffset, false, null);
	}

	public void init(final File file, long memoryOffset, final boolean showDialog, JFrame frame, boolean showDebugLoc, boolean showDebugInfoEntriesInCompileUnit) {
		this.showDialog = showDialog;
		this.showDebugLoc = showDebugLoc;
		this.showDebugInfoEntriesInCompileUnit = showDebugInfoEntriesInCompileUnit;
		final ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(file, memoryOffset, true);
		if (dwarfArrayList == null) {
			JOptionPane.showMessageDialog(frame, "Error loading elf", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		final JProgressBarDialog dialog = new JProgressBarDialog(frame, "Loading", true);
		dialog.progressBar.putClientProperty("ALIGN", SwingConstants.LEFT);
		dialog.progressBar.setIndeterminate(true);
		dialog.progressBar.setStringPainted(true);

		Thread longRunningThread = new Thread() {
			public void run() {
				for (final Dwarf dwarf : dwarfArrayList) {
					dwarfs.add(dwarf);
					if (dwarfArrayList == null) {
						System.err.println("dwarf init fail");
						return;
					}
					files.add(file);
					DwarfTreeNode node = new DwarfTreeNode(dwarf, root, null);
					root.children.add(node);

					// init section nodes
					final DwarfTreeNode sectionNodes = new DwarfTreeNode("section", node, null);
					node.children.add(sectionNodes);
					pool = Executors.newFixedThreadPool(maxPoolSize);
					for (final Elf_Shdr section : dwarf.sections) {
						pool.execute(new Runnable() {
							public void run() {
								if (showDialog) {
									dialog.progressBar.setString("Loading debug info : " + dwarf + ", section : " + section.getSection_name());
								}
								DwarfTreeNode sectionSubNode = new DwarfTreeNode(section.getSection_name() + ", offset: 0x" + Long.toHexString(section.getSh_offset().longValue()) + ", size: 0x"
										+ Long.toHexString(section.getSh_size().longValue()) + ", addr: 0x" + Long.toHexString(section.getSh_addr().longValue()), sectionNodes, section);
								String str = "<html><table>";
								str += "<tr><td>no.</td><td>:</td><td>" + section.getNumber() + "</td></tr>";
								str += "<tr><td>name</td><td>:</td><td>" + section.getSection_name() + "</td></tr>";
								str += "<tr><td>offset</td><td>:</td><td>0x" + Long.toHexString(section.getSh_offset().longValue()) + "</td></tr>";
								str += "<tr><td>size</td><td>:</td><td>0x" + Long.toHexString(section.getSh_size().longValue()) + "</td></tr>";
								str += "<tr><td>type</td><td>:</td><td>" + section.getSh_type() + "</td></tr>";
								str += "<tr><td>addr</td><td>:</td><td>0x" + Long.toHexString(section.getSh_addr().longValue()) + "</td></tr>";
								str += "<tr><td>addr align</td><td>:</td><td>" + section.getSh_addralign() + "</td></tr>";
								str += "<tr><td>ent. size</td><td>:</td><td>" + section.getSh_entsize() + "</td></tr>";
								str += "<tr><td>flags</td><td>:</td><td>" + section.getSh_flags() + "</td></tr>";
								str += "<tr><td>info</td><td>:</td><td>" + section.getSh_info() + "</td></tr>";
								str += "<tr><td>link</td><td>:</td><td>" + section.getSh_link() + "</td></tr>";
								str += "<tr><td>name idx</td><td>:</td><td>" + section.getSh_name() + "</td></tr>";
								str += "</table></html>";
								sectionSubNode.tooltip = str;
								sectionNodes.children.add(sectionSubNode);
							}
						});
					}
					while (dwarf.sections.size() != sectionNodes.children.size())
						;

					Collections.sort(sectionNodes.children, new Comparator<DwarfTreeNode>() {
						@Override
						public int compare(DwarfTreeNode o1, DwarfTreeNode o2) {
							Elf_Shdr c1 = (Elf_Shdr) o1.object;
							Elf_Shdr c2 = (Elf_Shdr) o2.object;
							return new Integer(c1.getNumber()).compareTo(new Integer(c2.getNumber()));
						}
					});
					// end init section nodes

					// init abbrev nodes
					final DwarfTreeNode abbrevNode = new DwarfTreeNode("abbrev", node, null);
					node.children.add(abbrevNode);

					final LinkedHashMap<Integer, LinkedHashMap<Integer, Abbrev>> abbrevList = dwarf.abbrevList;
					if (abbrevList != null) {
						pool = Executors.newFixedThreadPool(maxPoolSize);
						for (final Integer abbrevOffset : abbrevList.keySet()) {
							pool.execute(new Runnable() {
								public void run() {
									if (showDialog) {
										dialog.progressBar.setString("Loading debug info : " + dwarf + ", Abbrev offset : " + abbrevOffset);
									}
									DwarfTreeNode abbrevSubnode = new DwarfTreeNode("Abbrev offset=" + abbrevOffset, abbrevNode, null);
									abbrevNode.children.add(abbrevSubnode);
									LinkedHashMap<Integer, Abbrev> abbrevHashMap = abbrevList.get(abbrevOffset);
									for (Integer abbrevNo : abbrevHashMap.keySet()) {
										Abbrev abbrev = abbrevHashMap.get(abbrevNo);
										DwarfTreeNode abbrevSubnode2 = new DwarfTreeNode(abbrev.toString(), abbrevSubnode, abbrev);
										abbrevSubnode.children.add(abbrevSubnode2);
										for (AbbrevEntry entry : abbrev.entries) {
											DwarfTreeNode abbrevSubnode3 = new DwarfTreeNode(
													entry.at + ", " + entry.form + ", " + Definition.getATName(entry.at) + ", " + Definition.getFormName(entry.form),
													abbrevSubnode2, entry);
											abbrevSubnode2.children.add(abbrevSubnode3);
										}

									}

								}
							});
						}
						waitPoolFinish();

						Collections.sort(abbrevNode.children, new Comparator<DwarfTreeNode>() {
							@Override
							public int compare(DwarfTreeNode o1, DwarfTreeNode o2) {
								String c1 = o1.getText().split("=")[1];
								String c2 = o2.getText().split("=")[1];
								return new Integer(c1).compareTo(new Integer(c2));
							}
						});
					}
					// end init abbrev nodes

					// init compile unit nodes
					final DwarfTreeNode compileUnitNode = new DwarfTreeNode("compile unit", node, null);
					node.children.add(compileUnitNode);

					ArrayList<CompileUnit> compileUnits = dwarf.compileUnits;
					pool = Executors.newFixedThreadPool(maxPoolSize);
					for (final CompileUnit compileUnit : compileUnits) {
						pool.execute(new Runnable() {
							public void run() {
								final DwarfTreeNode compileUnitSubnode = new DwarfTreeNode(
										"0x" + Long.toHexString(compileUnit.DW_AT_low_pc) + " - " + "0x"
										+ Long.toHexString(compileUnit.DW_AT_high_pc - 1) + " - " + compileUnit.DW_AT_name + ", offset="
										+ compileUnit.abbrev_offset + ", length=" + Long.toHexString(compileUnit.length) + " (size " + compileUnit.addr_size + ")",
										compileUnitNode, compileUnit);
								compileUnitNode.children.add(compileUnitSubnode);

								// init headers
								final DwarfTreeNode headNode = new DwarfTreeNode("header", compileUnitSubnode, null);
								compileUnitSubnode.children.add(headNode);

								DwarfDebugLineHeader header = compileUnit.dwarfDebugLineHeader;
								DwarfTreeNode headerSubnode = new DwarfTreeNode(header.toString(), headNode, header);
								String str = "<html><table>";
								str += "<tr><td>offset</td><td>:</td><td>0x" + Long.toHexString(header.offset_size) + "</td></tr>";
								str += "<tr><td>total length</td><td>:</td><td>" + header.total_length + "</td></tr>";
								str += "<tr><td>DWARF Version</td><td>:</td><td>" + header.version + "</td></tr>";
								str += "<tr><td>Prologue Length</td><td>:</td><td>" + header.prologue_length + "</td></tr>";
								str += "<tr><td>Minimum Instruction Length</td><td>:</td><td>" + header.minimum_instruction_length + "</td></tr>";
								str += "<tr><td>Initial value of 'is_stmt'</td><td>:</td><td>" + (header.default_is_stmt ? 1 : 0) + "</td></tr>";
								str += "<tr><td>Line Base</td><td>:</td><td>" + header.line_base + "</td></tr>";
								str += "<tr><td>Line Range</td><td>:</td><td>" + header.line_range + "</td></tr>";
								str += "<tr><td>Opcode Base</td><td>:</td><td>" + header.opcode_base + "</td></tr>";
								str += "</table></html>";
								headerSubnode.tooltip = str;
								headNode.children.add(headerSubnode);

								DwarfTreeNode dirnamesNode = new DwarfTreeNode("dir name", headerSubnode, null);
								headerSubnode.children.add(dirnamesNode);
								for (String dir : header.dirnames) {
									dirnamesNode.children.add(new DwarfTreeNode(dir, dirnamesNode, null));
								}

								DwarfTreeNode filenamesNode = new DwarfTreeNode("file name", headerSubnode, null);
								headerSubnode.children.add(filenamesNode);
								for (DwarfHeaderFilename filename : header.filenames) {
									if (filename != null) {
										filenamesNode.children.add(new DwarfTreeNode(filename.file.getAbsolutePath(), filenamesNode, null));
									}
								}

								DwarfTreeNode lineInfoNode = new DwarfTreeNode("line info", headerSubnode, null);
								headerSubnode.children.add(lineInfoNode);
								for (DwarfLine line : header.lines) {
									DwarfTreeNode lineSubnode = new DwarfTreeNode("file_num=" + line.file_num + ", line_num:" + line.line_num + ", column_num=" + line.column_num
											+ ", address=0x" + line.address.toString(16), lineInfoNode, line);
									lineInfoNode.children.add(lineSubnode);
								}
								// end init headers

								for (final DebugInfoEntry debugInfoEntry : compileUnit.debugInfoEntries) {
									final DwarfTreeNode compileUnitDebugInfoNode = new DwarfTreeNode(debugInfoEntry.toString(), compileUnitSubnode, debugInfoEntry);
									compileUnitSubnode.children.add(compileUnitDebugInfoNode);

									Enumeration<String> e = Collections.enumeration(debugInfoEntry.debugInfoAbbrevEntries.keySet());
									while (e.hasMoreElements()) {
										String key = e.nextElement();
										DwarfTreeNode compileUnitDebugInfoAbbrevEntrySubnode = new DwarfTreeNode(debugInfoEntry.debugInfoAbbrevEntries.get(key).toString(),
												compileUnitDebugInfoNode, debugInfoEntry.debugInfoAbbrevEntries.get(key));
										compileUnitDebugInfoNode.children.add(compileUnitDebugInfoAbbrevEntrySubnode);
									}

									Collections.sort(compileUnitDebugInfoNode.children, new Comparator<DwarfTreeNode>() {
										@Override
										public int compare(DwarfTreeNode o1, DwarfTreeNode o2) {
											DebugInfoAbbrevEntry c1 = (DebugInfoAbbrevEntry) o1.object;
											DebugInfoAbbrevEntry c2 = (DebugInfoAbbrevEntry) o2.object;
											return Integer.valueOf(c1.position).compareTo(c2.position);
										}
									});
									if (showDebugInfoEntriesInCompileUnit) {
										addDebugInfoEntries(dialog, compileUnit, compileUnitDebugInfoNode, debugInfoEntry, 0);
									}
								}
							}
						});
					}

					waitPoolFinish();

					Collections.sort(compileUnitNode.children, new Comparator<DwarfTreeNode>() {
						@Override
						public int compare(DwarfTreeNode o1, DwarfTreeNode o2) {
							CompileUnit c1 = (CompileUnit) o1.object;
							CompileUnit c2 = (CompileUnit) o2.object;
							return new Integer((int) c1.DW_AT_low_pc).compareTo(new Integer((int) c2.DW_AT_low_pc));
						}
					});
					// end init compile unit nodes

					// init .eh_frame
					final DwarfTreeNode ehFrameTreeNode = new DwarfTreeNode(".eh_frame", node, null);
					node.children.add(ehFrameTreeNode);

					pool = Executors.newFixedThreadPool(maxPoolSize);
					for (final FrameChunk ehFrame : dwarf.ehFrames) {
						pool.execute(new Runnable() {
							public void run() {
								if (showDialog) {
									dialog.progressBar.setString("Loading .eh_frame : " + Long.toHexString(ehFrame.pc_begin_real) + " - "
											+ Long.toHexString(ehFrame.pc_begin_real + ehFrame.pc_range_real));
								}
								if (ehFrame.cieID != 0) {
									DwarfTreeNode ehFrameSubNode = new DwarfTreeNode(
											Long.toHexString(ehFrame.pc_begin_real) + " - " + Long.toHexString(ehFrame.pc_begin_real + ehFrame.pc_range_real), ehFrameTreeNode,
											ehFrame);

									// for (Object key :
									// ehFrame.fieDetails.keySet()) {
									for (int x = 0; x < ehFrame.fieDetailsKeys.size(); x++) {
										String key = ehFrame.fieDetailsKeys.get(x);
										Object objects[] = ehFrame.fieDetails.get(x);
										String s = "";
										for (Object object : objects) {
											if (!s.equals("")) {
												s += ", ";
											}
											s += object;
										}
										DwarfTreeNode ehFrameFieSubNode = new DwarfTreeNode(key + " : " + s, ehFrameSubNode, ehFrame);
										ehFrameSubNode.children.add(ehFrameFieSubNode);
									}

									ehFrameTreeNode.children.add(ehFrameSubNode);
								} else {
									DwarfTreeNode ehFrameSubNode = new DwarfTreeNode(
											Long.toHexString(ehFrame.pc_begin_real) + " - " + Long.toHexString(ehFrame.pc_begin_real + ehFrame.pc_range_real) + " CIE",
											ehFrameTreeNode, ehFrame);

									DwarfTreeNode ehFrameCieSubNode;

									ehFrameCieSubNode = new DwarfTreeNode("Version : " + ehFrame.version, ehFrameSubNode, ehFrame);
									ehFrameSubNode.children.add(ehFrameCieSubNode);

									ehFrameCieSubNode = new DwarfTreeNode("Augmentation : " + ehFrame.augmentation, ehFrameSubNode, ehFrame);
									ehFrameSubNode.children.add(ehFrameCieSubNode);

									ehFrameCieSubNode = new DwarfTreeNode("Code factor : " + ehFrame.code_factor, ehFrameSubNode, ehFrame);
									ehFrameSubNode.children.add(ehFrameCieSubNode);

									ehFrameCieSubNode = new DwarfTreeNode("Data factor : " + ehFrame.data_factor, ehFrameSubNode, ehFrame);
									ehFrameSubNode.children.add(ehFrameCieSubNode);

									ehFrameCieSubNode = new DwarfTreeNode("Return address column : " + ehFrame.ra, ehFrameSubNode, ehFrame);
									ehFrameSubNode.children.add(ehFrameCieSubNode);

									if (ehFrame.augmentationData != null) {
										String augmentationDataStr = "";
										for (byte b : ehFrame.augmentationData) {
											augmentationDataStr += b + ",";
										}
										ehFrameCieSubNode = new DwarfTreeNode("Augmentation data : " + augmentationDataStr, ehFrameSubNode, ehFrame);
									}
									ehFrameSubNode.children.add(ehFrameCieSubNode);

									// ehFrameFieSubNode = new
									// DwarfTreeNode("DW_CFA_def_cfa : " +
									// ehFrame.cfa_reg, ehFrameSubNode,
									// ehFrame);
									// ehFrameSubNode.children.add(ehFrameFieSubNode);
									//
									// ehFrameFieSubNode = new
									// DwarfTreeNode("DW_CFA_def_cfa : " +
									// ehFrame.cfa_offset, ehFrameSubNode,
									// ehFrame);
									// ehFrameSubNode.children.add(ehFrameFieSubNode);
									ehFrameTreeNode.children.add(ehFrameSubNode);
								}
							}
						});
					}

					waitPoolFinish();

					Collections.sort(ehFrameTreeNode.children, new Comparator<DwarfTreeNode>() {
						@Override
						public int compare(DwarfTreeNode o1, DwarfTreeNode o2) {
							Integer i1 = ((FrameChunk) o1.object).sequenceNo;
							Integer i2 = ((FrameChunk) o2.object).sequenceNo;
							return i1.compareTo(i2);
						}
					});
					// end init .eh_frame

					// init .debug_loc
					if (showDebugLoc) {
						final DwarfTreeNode debugLocTreeNode = new DwarfTreeNode(".debug_loc", node, null);
						node.children.add(debugLocTreeNode);

						int startIndex = 1;
						int endIndex = 2000;
						DwarfTreeNode tempNode = new DwarfTreeNode(startIndex + " - " + endIndex, node, null);
						debugLocTreeNode.children.add(tempNode);

						pool = Executors.newFixedThreadPool(maxPoolSize);
						for (final DebugLocEntry debugLocEntry : dwarf.debugLocEntries) {
//							pool.execute(new Runnable() {
//								public void run() {
							if (startIndex % 2000 == 0) {
								endIndex += 2000;
								tempNode = new DwarfTreeNode(startIndex + " - " + endIndex, node, null);
								debugLocTreeNode.children.add(tempNode);
							}
							if (showDialog) {
								dialog.progressBar.setString("Loading .debug_loc : " + debugLocEntry);
							}

							DwarfTreeNode debugLocChildNode = new DwarfTreeNode(debugLocEntry.toString(), debugLocTreeNode, debugLocEntry);
							//debugLocTreeNode.children.add(debugLocChildNode);
							tempNode.children.add(debugLocChildNode);
							startIndex++;
//								}
//							});
						}

//						waitPoolFinish();
//						QuantrDwarf.log("debugLocTreeNode=" + debugLocTreeNode.getChildCount());
//						Collections.sort(debugLocTreeNode.children, new Comparator<DwarfTreeNode>() {
//							@Override
//							public int compare(DwarfTreeNode o1, DwarfTreeNode o2) {
//								DebugLocEntry debugLocEntry1 = (DebugLocEntry) o1.object;
//								DebugLocEntry debugLocEntry2 = (DebugLocEntry) o2.object;
//								return Integer.valueOf(debugLocEntry1.offset).compareTo(Integer.valueOf(debugLocEntry2.offset));
//							}
//						});
					}
					// end init .debug_loc
				}

				expandFirstLevel();
			}
		};
		dialog.thread = longRunningThread;
		dialog.setVisible(true);
	}

	void waitPoolFinish() {
		pool.shutdown();
		try {
			if (!pool.awaitTermination(600, TimeUnit.SECONDS)) {
				pool.shutdownNow();
				if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
					System.err.println("Pool did not terminate");
				}
			}
		} catch (InterruptedException ie) {
			pool.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}

	void expandFirstLevel() {
		Enumeration<DwarfTreeNode> topLevelNodes = ((DwarfTreeNode) tree.getModel().getRoot()).children();
		while (topLevelNodes.hasMoreElements()) {
			DwarfTreeNode node = (DwarfTreeNode) topLevelNodes.nextElement();
			tree.expandPath(new TreePath(node.getPath()));
		}
	}

	private void addDebugInfoEntries(JProgressBarDialog dialog, final CompileUnit compileUnit, DwarfTreeNode node, DebugInfoEntry debugInfoEntry, int level) {
		if (showDialog) {
			//dialog.progressBar.setString("Loading debug info : cu, " + compileUnit.offset + ", " + new File(compileUnit.DW_AT_name).getName());
			dialog.progressBar.setString("Loading debug info : " + new File(compileUnit.DW_AT_name).getName());
//			QuantrDwarf.log(StringUtils.repeat("-", level) + compileUnit.DW_AT_name);
		}
		if (debugInfoEntry.debugInfoEntries.size() == 0) {
			return;
		}

		for (final DebugInfoEntry d : debugInfoEntry.debugInfoEntries) {
			//			QuantrDwarf.log(d);
			final DwarfTreeNode subNode = new DwarfTreeNode(d.toString(), node, d);
			node.children.add(subNode);

			//			pool.execute(new Runnable() {
			//				public void run() {
			Enumeration<String> e = Collections.enumeration(d.debugInfoAbbrevEntries.keySet());
			while (e.hasMoreElements()) {
				String key = e.nextElement();

				DwarfTreeNode compileUnitDebugInfoAbbrevEntrySubnode;
				DebugInfoAbbrevEntry debugInfoAbbrevEntry = d.debugInfoAbbrevEntries.get(key);
				//fuck QuantrDwarf.log(Integer.toHexString(debugInfoAbbrevEntry.position) + " = " + debugInfoAbbrevEntry.name);
				if (debugInfoAbbrevEntry.name.equals("DW_AT_decl_file")) {
					compileUnitDebugInfoAbbrevEntrySubnode = new DwarfTreeNode(
							debugInfoAbbrevEntry.toString() + ", "
							+ compileUnit.dwarfDebugLineHeader.filenames.get(Integer.parseInt(debugInfoAbbrevEntry.value.toString()) - 1).file.getAbsolutePath(),
							subNode, debugInfoAbbrevEntry);
				} else if (debugInfoAbbrevEntry.name.equals("DW_AT_type")) {
					int value = CommonLib.string2int("0x" + debugInfoAbbrevEntry.value.toString());
					String type = DwarfLib.getParameterType(compileUnit, value);
					if (type == null) {
						compileUnitDebugInfoAbbrevEntrySubnode = new DwarfTreeNode(debugInfoAbbrevEntry.toString(), subNode, debugInfoAbbrevEntry);
					} else {
						compileUnitDebugInfoAbbrevEntrySubnode = new DwarfTreeNode(debugInfoAbbrevEntry.toString() + ", " + type, subNode, debugInfoAbbrevEntry);
					}
				} else if (debugInfoAbbrevEntry.name.equals("DW_AT_location")) {
					String value = "";
					if (debugInfoAbbrevEntry.form == Definition.DW_FORM_block1) {
						byte bytes[] = (byte[]) debugInfoAbbrevEntry.value;
						value = Definition.getOPName(bytes[0]);
						value += " ";
						long l;
						if (bytes.length > 4) {
							l = CommonLib.getInt(bytes, 1);
						} else if (bytes.length > 2) {
							l = CommonLib.getShort(bytes[1], bytes[2]);
						} else {
							l = bytes[1];
						}
						value += "0x" + Long.toHexString(l);
					} else {
						String values[] = debugInfoAbbrevEntry.value.toString().split(",");
						if (values.length > 1) {
							value = Definition.getOPName(CommonLib.string2int(values[0]));
							value += " +" + values[1];
						} else {
							value = Definition.getOPName(CommonLib.string2int(values[0]));
						}
					}
					compileUnitDebugInfoAbbrevEntrySubnode = new DwarfTreeNode(debugInfoAbbrevEntry.toString() + ", " + value, subNode, debugInfoAbbrevEntry);
				} else {
					compileUnitDebugInfoAbbrevEntrySubnode = new DwarfTreeNode(debugInfoAbbrevEntry.toString(), subNode, debugInfoAbbrevEntry);
				}
				subNode.children.add(compileUnitDebugInfoAbbrevEntrySubnode);
			}

//			Collections.sort(subNode.children, new Comparator<DwarfTreeNode>() {
//				@Override
//				public int compare(DwarfTreeNode o1, DwarfTreeNode o2) {
//					DebugInfoAbbrevEntry c1 = (DebugInfoAbbrevEntry) o1.object;
//					DebugInfoAbbrevEntry c2 = (DebugInfoAbbrevEntry) o2.object;
//					return new Integer(c1.position).compareTo(new Integer(c2.position));
//				}
//			});
			addDebugInfoEntries(dialog, compileUnit, subNode, d, level++);
			//				}
			//			});
		}
	}

	public CompileUnit getCompileUnit(long address) {
		for (Dwarf dwarf : dwarfs) {
			CompileUnit cu = dwarf.getCompileUnit(address);
			if (cu != null) {
				return cu;
			}
		}
		return null;
	}
}

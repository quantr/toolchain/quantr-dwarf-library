package hk.quantr.dwarf;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.gui.QuantrDwarfPanel;
import hk.quantr.setting.library.QuantrSettingLibrary;
import hk.quantr.setting.library.annotation.QuantrSetting;
import hk.quantr.setting.library.annotation.SettingElement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestPeterDwarfJFrame extends javax.swing.JFrame {

	private JToolBar toolBar1;
	private JButton openButton;
	private QuantrDwarfPanel quantrDwarfPanel1;
	static String[] args;

	@QuantrSetting
	class Setting {

		@SettingElement
		public String path;
	}

	Setting setting = new Setting();

	public static void main(String[] args) {
		TestPeterDwarfJFrame.args = args;
		DwarfGlobal.debug = true;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				TestPeterDwarfJFrame inst = new TestPeterDwarfJFrame();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	public TestPeterDwarfJFrame() {
		super();
		//		addWindowListener(new WindowAdapter() {
		//			@Override
		//			public void windowOpened(WindowEvent e) {
		//				//				openButtonActionPerformed(null);
		//			}
		//		});
		QuantrDwarf.println("initGUI()");
		initGUI();
		QuantrDwarf.println("initGUI() end");
		if (args.length > 0) {
			quantrDwarfPanel1.init(new File(args[0]), 0, true, this, true, true);
		}
	}

	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			this.setTitle("Test Peter-dwarf Library");
			{
				toolBar1 = new JToolBar();
				getContentPane().add(toolBar1, BorderLayout.NORTH);
				{
					openButton = new JButton();
					toolBar1.add(openButton);
					openButton.setText("Open ELF");
					openButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							openButtonActionPerformed(evt);
						}
					});
				}
			}
			{
				quantrDwarfPanel1 = new QuantrDwarfPanel();
				getContentPane().add(quantrDwarfPanel1, BorderLayout.CENTER);
			}

			QuantrSettingLibrary.load("setting", setting);
			if (setting.path != null) {
				JButton temp = new JButton();
				toolBar1.add(temp);
				temp.setText(new File(setting.path).getName());
				TestPeterDwarfJFrame that = this;
				temp.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						quantrDwarfPanel1.init(new File(setting.path), 0, true, that, true, true);
					}
				});
			}
			pack();
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setSize(900, 750);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void openButtonActionPerformed(ActionEvent evt) {
		try {
			//File file = new File("/Users/peter/linux-4.0-rc5/");
			//File file = new File("../PeterI/kernel/kernel");
			JFileChooser chooser = new JFileChooser();
			chooser.showOpenDialog(this);

			File file = chooser.getSelectedFile();
			setting.path = file.getAbsolutePath();
			QuantrSettingLibrary.save("setting", setting);
			if (file != null) {
				quantrDwarfPanel1.init(file, 0, true, this, true, true);

				for (Dwarf dwarf : quantrDwarfPanel1.dwarfs) {
					QuantrDwarf.println(dwarf.getCompileUnitByFunction("itoa"));
				}
			}
		} catch (Exception ex) {
			Logger.getLogger(TestPeterDwarfJFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}

package hk.quantr.dwarf.dwarf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import hk.quantr.ar.AR;
import hk.quantr.ar.QuantrAR;
import hk.quantr.dwarf.QuantrDwarf;
import hk.quantr.dwarf.elf.Elf_Sym;
import hk.quantr.javalib.CommonLib;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.commons.lang3.ArrayUtils;

public class DwarfLib {

	private static final boolean WORDS_BIGENDIAN = ByteOrder.nativeOrder().equals(ByteOrder.BIG_ENDIAN);

	public static void main(String args[]) {
		long address = 0x1bd3fb0;
		final ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(new File("../PeterI/kernel/kernel"), 0, true);
		if (dwarfArrayList == null) {
			return;
		}
		HashMap<String, DwarfParameter> parameters = getParameters(dwarfArrayList, address);
		QuantrDwarf.printf("Function %s, Parameter size of %x is %d\n", getFunctionName(dwarfArrayList, address), address, parameters.size());
		for (String key : parameters.keySet()) {
			QuantrDwarf.println(key + " <=> " + parameters.get(key).type);
		}
	}

	public static ArrayList<String> getAllFunctionNames(ArrayList<Dwarf> dwarfArrayList) {
		ArrayList<String> functionNames = new ArrayList<String>();
		for (Dwarf dwarf : dwarfArrayList) {
			for (CompileUnit cu : dwarf.compileUnits) {
				ArrayList<DebugInfoEntry> subprogramDebugInfoEntries = cu.debugInfoEntries.get(0).getDebugInfoEntryByName("DW_TAG_subprogram");
				for (DebugInfoEntry subprogramDebugInfoEntry : subprogramDebugInfoEntries) {
					if (subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc") == null) {
						continue;
					}
					long subProgramAddress = (long) subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc").value;
					long subProgramHighAddress = (long) subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc").value + (Long) subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_high_pc").value;
					if (subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name") == null) {
						// not every DW_TAG_subprogram has a DW_AT_name
						continue;
					}
					functionNames.add((String) subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name").value);
				}
			}
		}
		return functionNames;
	}

	public static String getFunctionName(ArrayList<Dwarf> dwarfArrayList, long address) {
		if (dwarfArrayList != null) {
			for (Dwarf dwarf : dwarfArrayList) {
				for (CompileUnit cu : dwarf.compileUnits) {
					ArrayList<DebugInfoEntry> subprogramDebugInfoEntries = cu.debugInfoEntries.get(0).getDebugInfoEntryByName("DW_TAG_subprogram");
					for (DebugInfoEntry subprogramDebugInfoEntry : subprogramDebugInfoEntries) {
						if (subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc") == null) {
							continue;
						}
						long subProgramAddress = (long) subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc").value;
						long subProgramHighAddress = subProgramAddress + (Long) subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_high_pc").value - 1;
						if (subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name") == null) {
							// not every DW_TAG_subprogram has a DW_AT_name
							continue;
						}
						if (subProgramAddress <= address && address <= subProgramHighAddress) {
							//QuantrDwarf.log(subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name").value);
							return (String) subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name").value;
						}
					}
				}
				for (Elf_Sym symbol : dwarf.symbols) {
					if (symbol.getST_value() == address && symbol.getName() != null && !symbol.getName().equals("")) {
						return symbol.getName();
					}
				}
			}
		}

		return null;
	}

	public static CompileUnit getCompileUnit(ArrayList<Dwarf> dwarfArrayList, long address) {
		if (dwarfArrayList != null) {
			for (Dwarf dwarf : dwarfArrayList) {
				CompileUnit cu = getCompileUnit(dwarf, address);
				if (cu != null) {
					return cu;
				}
			}
		}

		return null;
	}

	public static CompileUnit getCompileUnit(Dwarf dwarf, long address) {
		for (CompileUnit cu : dwarf.compileUnits) {
			ArrayList<DebugInfoEntry> subprogramDebugInfoEntries = cu.debugInfoEntries.get(0).getDebugInfoEntryByName("DW_TAG_subprogram");
			for (DebugInfoEntry subprogramDebugInfoEntry : subprogramDebugInfoEntries) {
				if (subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc") == null) {
					continue;
				}
				if (Dwarf.showDebugMessage) {
					QuantrDwarf.printf("%x <= %x <= %x\n", cu.DW_AT_low_pc, address, cu.DW_AT_high_pc - 1);
				}
				if (cu.DW_AT_low_pc <= address && address <= cu.DW_AT_high_pc - 1) {
					//QuantrDwarf.log(subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name").value);
					return cu;
				}
			}
		}

		return null;
	}

	public static HashMap<String, DwarfParameter> getParameters(ArrayList<Dwarf> dwarfArrayList, long address) {
		HashMap<String, DwarfParameter> ht = new HashMap<>();
		for (Dwarf dwarf : dwarfArrayList) {
			for (CompileUnit cu : dwarf.compileUnits) {
				//if (cu.DW_AT_low_pc <= address && address <= (cu.DW_AT_low_pc + cu.DW_AT_high_pc - 1)) {
				ArrayList<DebugInfoEntry> subprogramDebugInfoEntries = cu.debugInfoEntries.get(0).getDebugInfoEntryByName("DW_TAG_subprogram");
				for (DebugInfoEntry subprogramDebugInfoEntry : subprogramDebugInfoEntries) {
					if (subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc") == null) {
						continue;
					}
					if (subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name") == null) {
						// not every DW_TAG_subprogram has a DW_AT_name
						continue;
					}

					long subProgramAddress = (long) subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc").value;
					long subProgramHighAddress = (long) subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc").value
							+ (Integer) subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_high_pc").value - 1;

					//QuantrDwarf.log(subprogramDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name").value);
					if (subProgramAddress <= address && address <= subProgramHighAddress) {
						//CIE
						long cfsBaseOffset = -1;
						for (int x = 0; x < dwarf.ehFrames.get(0).fieDetailsKeys.size(); x++) {
							if (dwarf.ehFrames.get(0).fieDetailsKeys.get(x).equals("DW_CFA_def_cfa")) {
								cfsBaseOffset = (long) dwarf.ehFrames.get(0).fieDetails.get(x)[2];
								break;
							}
						}
						//CIE end

						ArrayList<DebugInfoEntry> parameters = subprogramDebugInfoEntry.getDebugInfoEntryByName("DW_TAG_formal_parameter");
						for (DebugInfoEntry parameterDebugInfoEntry : parameters) {
							if (parameterDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name") == null) {
								continue;
							}
							String name = (String) parameterDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name").value;
							//								QuantrDwarf.log(name);
							DebugInfoAbbrevEntry locationdebugInfoAbbrevEntry = parameterDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_location");
							String registerName = null;
							long offset = 0;
							if (locationdebugInfoAbbrevEntry.form == Definition.DW_FORM_exprloc) {
								String values[] = locationdebugInfoAbbrevEntry.value.toString().split(",");
								registerName = Definition.getOPName(CommonLib.string2int(values[0]));

								//								QuantrDwarf.log(parameterDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_name").value);
								//								QuantrDwarf.log(parameterDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_location").value);
								//								QuantrDwarf.log("values[0]=" + values[0]);
								if (values.length > 1) {
									offset = Long.parseLong(values[1]);
								}
							} else if (locationdebugInfoAbbrevEntry.form == Definition.DW_FORM_sec_offset) {
								String values[] = locationdebugInfoAbbrevEntry.value.toString().split(",");
								DebugLocEntry debugLocEntry = DwarfLib.getDebugLocEntry(dwarf, CommonLib.string2int(values[0]));
								if (debugLocEntry == null) {
									//QuantrDwarf.log("debugLocEntry==null, address=%x, %s, %s, %x\n", address, values[0], locationdebugInfoAbbrevEntry.value, subProgramAddress);
									//we need to continue, because in c, a function can be located in different compile unit, some compile units are for .h and some for .c, thats why we need to "continue" to loop all compile units
									continue;
								}
								registerName = Definition.getOPName(debugLocEntry.unsignedBlocks[0]);
								//									QuantrDwarf.log("debugLocEntry.blocks[0]=" + debugLocEntry.unsignedBlocks[0]);
								//									QuantrDwarf.log("debugLocEntry=" + debugLocEntry);
								//									QuantrDwarf.log("registerName=" + registerName);
								if (registerName == null) {
									System.exit(1);
								}
								if (registerName.equals("DW_OP_fbreg")) {
									offset = debugLocEntry.unsignedBlocks[1];
								}
							} else {
								System.err.println("Not support form=" + locationdebugInfoAbbrevEntry.form);
							}
							if (registerName == null) {
								System.exit(1);
							}
							if (registerName.equals("DW_OP_fbreg")) {
								//									QuantrDwarf.log(name + ", " + (cfsBaseOffset + offset));
								offset = cfsBaseOffset + offset;
							} else {
								//									QuantrDwarf.log("not support register=" + registerName);
								//System.exit(500);
							}
							ht.put(name,
									new DwarfParameter(name, registerName,
											DwarfLib.getParameterType(cu, (Integer) parameterDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_type").value),
											DwarfLib.getParameterSize(cu, (Integer) parameterDebugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_type").value), offset));
						}

						return ht;
					}
				}
				//}
			}
		}
		return null;
	}

	public static ArrayList<Dwarf> init(File file, long memoryOffset, boolean showDebugMessage) {
		if (showDebugMessage) {
			QuantrDwarf.println("init()");
		}
		ArrayList<Dwarf> dwarfArrayList = new ArrayList<Dwarf>();
		if (isArchive(file)) {
			QuantrAR quantrAR = new QuantrAR();
			ArrayList<AR> data = quantrAR.init(file);
			if (data != null) {
				for (AR ar : data) {
					try {
						File temp = File.createTempFile("peterDwarf", ".peterDwarf");
						FileOutputStream out = new FileOutputStream(temp);
						out.write(ar.bytes);
						out.close();
						Dwarf dwarf = new Dwarf();

						if (showDebugMessage) {
							QuantrDwarf.println("   ->  initELF " + ar.filename);
						}
						int r = dwarf.initElf(temp, ar.filename, memoryOffset, showDebugMessage);
						temp.delete();
						if (r > 0 && r != 24) {
							continue;
						} else {
							dwarfArrayList.add(dwarf);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			Dwarf dwarf = new Dwarf();
			if (showDebugMessage) {
				QuantrDwarf.println("   ->  initELF " + file.getName());
			}
			int r = dwarf.initElf(file, null, memoryOffset, showDebugMessage);
			if (r > 0) {
				System.err.println("Error code : " + r);
				return null;
			}

			dwarfArrayList.add(dwarf);
		}
		if (showDebugMessage) {
			QuantrDwarf.println("init() end");
		}
		return dwarfArrayList;
	}

	public static boolean isArchive(File file) {
		InputStream is;
		try {
			is = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		try {
			if (is.read() != 0x21 || is.read() != 0x3c || is.read() != 0x61 || is.read() != 0x72 || is.read() != 0x63 || is.read() != 0x68 || is.read() != 0x3e) {
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public static int readUHalf(RandomAccessFile f) throws IOException {
		if (WORDS_BIGENDIAN) {
			return f.readUnsignedShort();
		} else {
			return f.readUnsignedByte() | (f.readUnsignedByte() << 8);
		}
	}

	public static int readWord(RandomAccessFile f) throws IOException {
		if (WORDS_BIGENDIAN) {
			return f.readInt();
		} else {
			return (f.readUnsignedByte() | (f.readUnsignedByte() << 8) | (f.readUnsignedByte() << 16) | (f.readUnsignedByte() << 24));
		}
	}

	public static long readUWord(RandomAccessFile f) throws IOException {
		if (WORDS_BIGENDIAN) {
			return (long) f.readInt() & 0xFFFFFFFFL;
		} else {
			long l = (f.readUnsignedByte() | (f.readUnsignedByte() << 8) | (f.readUnsignedByte() << 16) | (f.readUnsignedByte() << 24));
			return (l & 0xFFFFFFFFL);
		}
	}

	public static BigInteger readU64Bits(RandomAccessFile f) throws IOException {
		if (WORDS_BIGENDIAN) {
			System.err.println("unsupport WORDS_BIGENDIAN");
			return BigInteger.valueOf(-1L);
		} else {
			byte b[] = new byte[8];
			f.readFully(b);
			ArrayUtils.reverse(b);
			return new BigInteger(b);
		}
	}

	public static void printBytes(byte[] bytes) {
		for (int x = 0; x < bytes.length; x++) {
			QuantrDwarf.printf("%02x ", bytes[x]);

			if ((x + 1) % 16 == 0) {
				QuantrDwarf.println();
			} else if ((x + 1) % 8 == 0) {
				QuantrDwarf.print(" - ");
			}
		}
	}

	public static void printByteBuffer(ByteBuffer byteBuffer) {
		int position = byteBuffer.position();
		int x = 0;
		while (byteBuffer.hasRemaining()) {
			QuantrDwarf.printf("%02x ", byteBuffer.get());
			//			QuantrDwarf.log("%c", byteBuffer.get());
			if (x == 7) {
				QuantrDwarf.print(" - ");
			} else if (x == 15) {
				QuantrDwarf.println();
				x = 0;
				continue;
			}
			x++;
		}
		QuantrDwarf.println();
		byteBuffer.position(position);
	}

	public static String getString(ByteBuffer buf) {
		int pos = buf.position();
		int len = 0;
		while (buf.get() != 0) {
			len++;
		}
		byte[] bytes = new byte[len];
		buf.position(pos);
		buf.get(bytes);
		buf.get();
		return new String(bytes);
	}

	public static long SKIP_ULEB(ByteBuffer buffer) {
		long len = getULEB128(buffer);
		return len;
	}

	public static long getULEB128(ByteBuffer buffer) {
		long val = 0;
		byte b;
		int shift = 0;

		while (true) {
			b = buffer.get();
			val |= ((long) (b & 0x7f)) << shift;
			if ((b & 0x80) == 0) {
				break;
			}
			shift += 7;
		}

		return val;
	}

	public static int getULEB128Count(ByteBuffer buffer) {
		long val = 0;
		byte b;
		int shift = 0;

		int count = 0;

		while (true) {
			b = buffer.get();
			count++;
			val |= ((long) (b & 0x7f)) << shift;
			if ((b & 0x80) == 0) {
				break;
			}
			shift += 7;
		}

		return count;
	}

	public static int getSLEB128(ByteBuffer buffer) {
		int result = 0;
		for (int i = 0; i < 5; i++) {
			byte b = buffer.get();
			result |= ((b & 0x7f) << (7 * i));
			if ((b & 0x80) == 0) {
				int s = 32 - (7 * (i + 1));
				result = (result << s) >> s;
				break;
			}
		}
		return result;
	}

	public static int readSignedLeb128_fuck(ByteBuffer buffer) {
		int result = 0;
		int cur;
		int count = 0;
		int signBits = -1;

		do {
			cur = buffer.get() & 0xff;
			result |= (cur & 0x7f) << (count * 7);
			signBits <<= 7;
			count++;
		} while (((cur & 0x80) == 0x80) && count < 5);

		if ((cur & 0x80) == 0x80) {
			System.out.println("FUCK");
			System.exit(1001);
		}

		// Sign extend if appropriate
		if (((signBits >> 1) & result) != 0) {
			result |= signBits;
		}

		return result;
	}

	public static int getSLEB128Count(ByteBuffer buffer) {
		int result = 0;
		int count = 0;
		for (int i = 0; i < 5; i++) {
			byte b = buffer.get();
			count++;
			result |= ((b & 0x7f) << (7 * i));
			if ((b & 0x80) == 0) {
				int s = 32 - (7 * (i + 1));
				result = (result << s) >> s;
				break;
			}
		}
		return count;
	}

	public static String getString(ByteBuffer buf, int offset) {
		try {
			buf.position(offset);

			byte temp;
			String r = "";
			while (buf.hasRemaining()) {
				temp = buf.get();
				if (temp == 0) {
					break;
				}
				r += (char) temp;
			}
			return r;
		} catch (Exception ex) {
			//			ex.printStackTrace();
			return null;
		}
	}

	public static BigInteger getAddress(ArrayList<Dwarf> dwarfArrayList, String filename, long lineNo) {
		if (dwarfArrayList != null) {
			for (Dwarf dwarf : dwarfArrayList) {
				for (CompileUnit cu : dwarf.compileUnits) {
//				QuantrDwarf.log(cu);
					if (cu.DW_AT_name.equals(filename)) {
						DwarfDebugLineHeader header = cu.dwarfDebugLineHeader;
						for (DwarfLine line : header.lines) {
							if (header.filenames.get((int) line.file_num).file.getName().equals(filename)) {
//						QuantrDwarf.log(header.filenames.get((int) line.file_num).file.getName() + " - " + line.line_num + " = " + line.address.toString(16));
								if (lineNo == line.line_num) {
									return line.address;
								}
							}
						}
					}
				}
			}
		}
		return BigInteger.valueOf(-1l);
	}

	public static HashMap<Integer, BigInteger> getAddresses(ArrayList<Dwarf> dwarfArrayList, String filename) {
		HashMap<Integer, BigInteger> ht = new HashMap<>();
		if (dwarfArrayList != null) {
			for (Dwarf dwarf : dwarfArrayList) {
				for (CompileUnit cu : dwarf.compileUnits) {
					if (cu.DW_AT_name.equals(filename)) {
						DwarfDebugLineHeader header = cu.dwarfDebugLineHeader;
						for (DwarfLine line : header.lines) {
							if (header.filenames.get((int) line.file_num).file.getName().equals(filename)) {
								ht.put(line.line_num, line.address);
							}
						}
					}
				}
			}
		}
		return ht;
	}

	public void printHeader(DwarfDebugLineHeader header) {
		QuantrDwarf.println("total_length: " + header.total_length);
		QuantrDwarf.println("version: " + header.version);
		QuantrDwarf.println("prologue_length: " + header.prologue_length);
		QuantrDwarf.println("minimum_instruction_length: " + header.minimum_instruction_length);
		QuantrDwarf.println("default_is_stmt: " + header.default_is_stmt);
		QuantrDwarf.println("line_base: " + header.line_base);
		QuantrDwarf.println("line_range: " + header.line_range);
		QuantrDwarf.println("opcode_base: " + header.opcode_base);
		QuantrDwarf.print("standard_opcode_lengths: { ");
		QuantrDwarf.print(header.standard_opcode_lengths[0]);
		QuantrDwarf.print(header.standard_opcode_lengths[1]);
		QuantrDwarf.print(header.standard_opcode_lengths[2]);
		QuantrDwarf.print(header.standard_opcode_lengths[3]);
		QuantrDwarf.print(header.standard_opcode_lengths[4]);
		QuantrDwarf.print(header.standard_opcode_lengths[5]);
		QuantrDwarf.print(header.standard_opcode_lengths[6]);
		QuantrDwarf.print(header.standard_opcode_lengths[7]);
		QuantrDwarf.print(header.standard_opcode_lengths[8]);
		QuantrDwarf.print(header.standard_opcode_lengths[9]);
		QuantrDwarf.print(header.standard_opcode_lengths[10]);
		QuantrDwarf.print(header.standard_opcode_lengths[11]);
		QuantrDwarf.println(" }");
	}

	public static DebugInfoEntry getSubProgram(ArrayList<Dwarf> dwarfArrayList, long address) {
		if (dwarfArrayList != null) {
			for (Dwarf dwarf : dwarfArrayList) {
				for (CompileUnit compileUnit : dwarf.compileUnits) {
					for (DebugInfoEntry debugInfoEntry : compileUnit.debugInfoEntries) {
						DebugInfoEntry result = searchSubProgram(debugInfoEntry, address);
						if (result != null) {
							return result;
						}
					}
				}
			}
		}
		return null;
	}

	private static DebugInfoEntry searchSubProgram(DebugInfoEntry debugInfoEntry, long address) {
		if (debugInfoEntry.name.equals("DW_TAG_subprogram")) {
			if (debugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc") != null && (long) debugInfoEntry.debugInfoAbbrevEntries.get("DW_AT_low_pc").value == address) {
				return debugInfoEntry;
			}
		}

		for (DebugInfoEntry d : debugInfoEntry.debugInfoEntries) {
			DebugInfoEntry result = searchSubProgram(d, address);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

	public static String getParameterType(CompileUnit compileUnit, int value) {
		DebugInfoEntry temp = compileUnit.getDebugInfoEntryByPosition(value);

		if (temp != null && temp.name.equals("DW_TAG_union_type")) {
			return "union";
		} else if (temp != null && temp.name.equals("DW_TAG_enumeration_type")) {
			return "enum";
		}
		DebugInfoAbbrevEntry debugInfoAbbrevEntry = null;
		if (temp != null) {
			debugInfoAbbrevEntry = temp.debugInfoAbbrevEntries.get("DW_AT_name");
			if (debugInfoAbbrevEntry == null) {
				debugInfoAbbrevEntry = temp.debugInfoAbbrevEntries.get("DW_AT_type");

				if (debugInfoAbbrevEntry == null) {
					return null;
				}
				if (value != CommonLib.string2int("0x" + debugInfoAbbrevEntry.value)) {
					return getParameterType(compileUnit, CommonLib.string2int("0x" + debugInfoAbbrevEntry.value));
				}
			}
		} else {
			return null;
		}

		String type = debugInfoAbbrevEntry.value.toString();
		return type;
	}

	public static int getParameterSize(CompileUnit compileUnit, int value) {
		DebugInfoEntry temp = compileUnit.getDebugInfoEntryByPosition(value);

		DebugInfoAbbrevEntry debugInfoAbbrevEntry = null;
		if (temp != null) {
			debugInfoAbbrevEntry = temp.debugInfoAbbrevEntries.get("DW_AT_byte_size");
			if (debugInfoAbbrevEntry == null) {
				debugInfoAbbrevEntry = temp.debugInfoAbbrevEntries.get("DW_AT_type");

				if (debugInfoAbbrevEntry == null) {
					return -1;
				}
				return getParameterSize(compileUnit, CommonLib.string2int("0x" + debugInfoAbbrevEntry.value));
			}
		} else {
			return -1;
		}

		int size = Integer.parseInt(debugInfoAbbrevEntry.value.toString());
		return size;
	}

	public static DebugLocEntry getDebugLocEntry(Dwarf dwarf, int offset) {
		for (DebugLocEntry debugLocEntry : dwarf.debugLocEntries) {
			if (debugLocEntry.offset == offset) {
				return debugLocEntry;
			}
		}
		return null;
	}
}

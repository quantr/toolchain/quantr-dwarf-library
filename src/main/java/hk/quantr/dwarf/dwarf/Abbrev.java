package hk.quantr.dwarf.dwarf;

import java.util.ArrayList;

public class Abbrev {
	public int number;
	public int tag;
	public boolean has_children;
	public ArrayList<AbbrevEntry> entries = new ArrayList<AbbrevEntry>();

	public String toString() {
		return number + ": " + Definition.getTagName(tag) + ", " + (has_children ? "has children" : "no children");
	}
}

package hk.quantr.dwarf.dwarf;

import java.util.ArrayList;
import java.util.HashMap;

public class DebugInfoEntry {
	public int position;
	public int abbrevNo;

	public HashMap<String, DebugInfoAbbrevEntry> debugInfoAbbrevEntries = new HashMap<>();
	public String name;

	public ArrayList<DebugInfoEntry> debugInfoEntries = new ArrayList<>();

	@Override
	public String toString() {
		return "0x" + Integer.toHexString(position) + ", " + name + ", abbrevNo=" + abbrevNo;
	}

	public ArrayList<DebugInfoEntry> getDebugInfoEntryByName(String name) {
		if (debugInfoEntries == null) {
			return null;
		}
		ArrayList<DebugInfoEntry> r = new ArrayList<DebugInfoEntry>();
		for (DebugInfoEntry debugInfoEntry : debugInfoEntries) {
			if (debugInfoEntry.name.equals(name)) {
				r.add(debugInfoEntry);
			}
		}
		return r;
	}

}

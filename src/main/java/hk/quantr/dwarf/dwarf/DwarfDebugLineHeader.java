package hk.quantr.dwarf.dwarf;

import java.util.ArrayList;

public class DwarfDebugLineHeader {

	public long offset;
	public long offset_size;
	public long total_length;
	public int version;
	public long prologue_length;
	public int minimum_instruction_length;
	public int max_ops_per_insn;
	public boolean default_is_stmt;
	public byte line_base;
	public int line_range;
	public int opcode_base;
	public byte[] standard_opcode_lengths;
	public ArrayList<String> dirnames = new ArrayList();
	public ArrayList<DwarfHeaderFilename> filenames = new ArrayList();
	public ArrayList<DwarfLine> lines = new ArrayList();
	public int address_size;
	public int segment_size;

	public String toString() {
		//		QuantrDwarf.log("Offset:                      0x%x\n", offset);
		//		QuantrDwarf.log("Length:                      %d\n", total_length);
		//		QuantrDwarf.log("DWARF Version:               %d\n", version);
		//		QuantrDwarf.log("Prologue Length:             %d\n", prologue_length);
		//		QuantrDwarf.log("Minimum Instruction Length:  %d\n", minimum_instruction_length);
		//		QuantrDwarf.log("Initial value of 'is_stmt':  %d\n", default_is_stmt ? 1 : 0);
		//		QuantrDwarf.log("Line Base:                   %d\n", line_base);
		//		QuantrDwarf.log("Line Range:                  %d\n", line_range);
		//		QuantrDwarf.log("Opcode Base:                 %d\n", opcode_base);

		return "offset=0x" + Long.toHexString(offset_size) + ", Length=" + total_length + ", DWARF Version=" + version + ", Prologue Length=" + prologue_length
				+ ", Minimum Instruction Length=" + minimum_instruction_length + ", Maximum Ops per Instruction=" + max_ops_per_insn + ", Initial value of 'is_stmt'=" + (default_is_stmt ? 1 : 0) + ", Line Base=" + line_base
				+ ", Line Range=" + line_range + ", Opcode Base=" + opcode_base;
	}

}

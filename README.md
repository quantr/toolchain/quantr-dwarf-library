Java library to read [DWARF](https://en.wikipedia.org/wiki/DWARF) format out of ELF file. This library not support DWARF 5. Search dwarf.c in binutils for ">= 5", then you know how complex it is.

# Author

Peter <peter@quantr.hk>

# How to use this library in your code

```
Dwarf dwarf = new Dwarf();
dwarf.initElf(new File("src/test/resources/riscv/xv6/kernel"), "kernel.elf", 0, true);

for (CompileUnit cu : dwarf.compileUnits) {
	System.out.println(cu);
	for (DebugInfoEntry entry : cu.debugInfoEntries) {
		System.out.println("\t" + entry);

		for (DebugInfoEntry entry2 : entry.debugInfoEntries) {
			System.out.println("\t\t" + entry2);
		}
	}
}
```

Result:

```
  Compilation Unit @ offset 0x0:
  	   Name:  kernel/start.c
  	   Length:        0x4f9 (32-bit)
  	   Version:       4
  	   Abbrev Offset: 0
  	   Pointer Size:  8
  	   Low pc:  2147483676
  	   High pc:  534

	0xb, DW_TAG_compile_unit, abbrevNo=1
		0x2d, DW_TAG_base_type, abbrevNo=2
		0x34, DW_TAG_base_type, abbrevNo=2
		0x3b, DW_TAG_base_type, abbrevNo=2
		0x42, DW_TAG_typedef, abbrevNo=3
		0x4e, DW_TAG_base_type, abbrevNo=2
		0x55, DW_TAG_pointer_type, abbrevNo=4
		0x5b, DW_TAG_array_type, abbrevNo=5
		0x6c, DW_TAG_base_type, abbrevNo=2
		0x73, DW_TAG_variable, abbrevNo=7
		0x8a, DW_TAG_array_type, abbrevNo=5
		0xa0, DW_TAG_variable, abbrevNo=9
		0xb6, DW_TAG_subprogram, abbrevNo=10
		0x1f0, DW_TAG_base_type, abbrevNo=20
		0x1f7, DW_TAG_subprogram, abbrevNo=10
		0x39e, DW_TAG_subprogram, abbrevNo=23
		0x3b8, DW_TAG_subprogram, abbrevNo=25
		0x3d0, DW_TAG_subprogram, abbrevNo=25
		0x3e8, DW_TAG_subprogram, abbrevNo=25
		0x400, DW_TAG_subprogram, abbrevNo=25
		0x418, DW_TAG_subprogram, abbrevNo=25
		0x430, DW_TAG_subprogram, abbrevNo=25
		0x448, DW_TAG_subprogram, abbrevNo=27
		0x464, DW_TAG_subprogram, abbrevNo=25
		0x47c, DW_TAG_subprogram, abbrevNo=27
		0x498, DW_TAG_subprogram, abbrevNo=25
		0x4b0, DW_TAG_subprogram, abbrevNo=25
		0x4c8, DW_TAG_subprogram, abbrevNo=27
		0x4e4, DW_TAG_subprogram, abbrevNo=29
  Compilation Unit @ offset 0x4fd:
  	   Name:  kernel/console.c
  	   Length:        0xb92 (32-bit)
  	   Version:       4
  	   Abbrev Offset: 436
  	   Pointer Size:  8
  	   Low pc:  2147483892
  	   High pc:  2376
```

# GUI

run TestPeterDwarfJFrame.java

![](https://www.quantr.foundation/wp-content/uploads/2024/04/quantr-dwarf-2024-04-13-at-7.29.19-AM.png)

# Command usage

## List all compile units

```
java -jar target/quantr-dwarf-2.0.jar src/test/resources/riscv/xv6/kernel listCompileUnit
```

Result:

```
  Compilation Unit @ offset 0x0:
  	   Name:  kernel/start.c
  	   Length:        0x4f9 (32-bit)
  	   Version:       4
  	   Abbrev Offset: 0
  	   Pointer Size:  8
  	   Low pc:  2147483676
  	   High pc:  534

  Compilation Unit @ offset 0x4fd:
  	   Name:  kernel/console.c
  	   Length:        0xb92 (32-bit)
  	   Version:       4
  	   Abbrev Offset: 436
  	   Pointer Size:  8
  	   Low pc:  2147483892
  	   High pc:  2376
```

